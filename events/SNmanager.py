import logging
logger = logging.getLogger("SNmanager")

import usersmanager
import dbmanager
import globals
from  pprint import pprint

def update_trust(user, mentioned, retweeted, favorited, decay=0):
    """ Update user interactions"""
    
    db = dbmanager.DBmanager(globals.TRUSTDB_FILE)
    
    for idmentioned in mentioned:
        db.addmention(user, idmentioned, decay)
    for idretweeted in retweeted:
        db.addretweet(user, idretweeted, decay)
    for idfavorited in favorited:
        db.addfavorite(user, idfavorited, decay)

    db.dbcommit()
    db.dbclose()

def get_trust(fromid, toid):
    """Get how much fromid trusts on toid"""
    
    db = dbmanager.DBmanager(globals.TRUSTDB_FILE)
    trust = db.gettrust(fromid, toid)
    return trust

def delete_irrelevant_users():
    """Delete old interactions that have been reduced for the decay factor"""

    threshold = globals.TRUST_THRESHOLD

    db = dbmanager.DBmanager(globals.TRUSTDB_FILE)

    # Normalize matrices to cast interactions into probabilities of interaction
    db.normalize_matrix("mentions")
    db.normalize_matrix("retweets")
    db.normalize_matrix("favorites")

# Delete users using a threshold on interaction level
#    db.con.execute("DELETE FROM mentions WHERE count < ?", (threshold,))
#    db.con.execute("DELETE FROM retweets WHERE count < ?", (threshold,))
#    db.con.execute("DELETE FROM favorites WHERE count < ?", (threshold,))

    users = usersmanager.get_users()
    for user in users:
        print "Truncating node ", user
        # Queries to get information on how many users are being deleted   
        u1bottom = db.con.execute("SELECT * FROM mentions WHERE fromid=? AND rowid NOT IN (SELECT rowid FROM mentions WHERE fromid=? ORDER BY count DESC LIMIT 100) ORDER BY count DESC", (user, user)).fetchall()
        u2bottom = db.con.execute("SELECT count FROM retweets WHERE fromid=? AND rowid NOT IN (SELECT rowid FROM retweets WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,user)).fetchall()
        u3bottom = db.con.execute("SELECT count FROM favorites WHERE fromid=? AND rowid NOT IN (SELECT rowid FROM favorites WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,user)).fetchall()
        u1top = db.con.execute("SELECT * FROM mentions WHERE fromid=? AND rowid IN (SELECT rowid FROM mentions WHERE fromid=? ORDER BY count DESC LIMIT 100) ORDER BY count DESC", (user,user)).fetchall()
        u2top = db.con.execute("SELECT * FROM retweets WHERE fromid=? AND rowid IN (SELECT rowid FROM retweets WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,user)).fetchall()
        u3top = db.con.execute("SELECT * FROM favorites WHERE fromid=? AND rowid IN (SELECT rowid FROM favorites WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,user)).fetchall()
        irrelevant = len(u1bottom) + len(u2bottom) + len(u3bottom)
        logger.debug("deleting " + str(irrelevant) + " irrelevant users")
        relevant = len(u1top) + len(u2top) + len(u3top)
        total = relevant + irrelevant
        logger.info("deleting " + str(irrelevant) + "/" + str(total) +  " irrelevant users")
        logger.info("keeping " + str(relevant) +  "/" + str(total) + " relevant users")  

        # Truncate from the 100th user on interaction level at every node
        db.con.execute("DELETE FROM mentions WHERE fromid=? AND rowid NOT IN (SELECT rowid FROM mentions WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,user))
        db.con.execute("DELETE FROM retweets WHERE rowid NOT IN (SELECT rowid FROM retweets WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,))
        db.con.execute("DELETE FROM favorites WHERE rowid NOT IN (SELECT rowid FROM favorites WHERE fromid=? ORDER BY count DESC LIMIT 100)", (user,))

    db.dbcommit()
    db.dbclose()

    
