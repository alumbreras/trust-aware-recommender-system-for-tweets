# -*- coding: utf-8 -*-

import logging
import logging.config
logging.config.fileConfig('./conf/logging.conf')
logger = logging.getLogger("classifier")

from os.path import *
from utils import basepath
import globals
import re
import random
import codecs
import utils
import math
from pprint import pprint

from nltk import * #importar solo porter o snowball
from nltk.corpus import stopwords
from nltk import word_tokenize, wordpunct_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.classify import weka

from sklearn import svm
from pattern.vector import Document, Corpus, HIERARCHICAL

from operator import itemgetter, attrgetter

import matplotlib.pyplot as plt
import numpy

class Dataset():

    instances = [] # each instance is a tuple (featureset, label)
    dictionary = []
    dictionary_tf = {}
    pos_corpus = [] # list of words in positive tweets (for tf purposes)
    neg_corpus = []

    # features in dictionary
    train_set = [] 
    test_set = []
    word_representation = 'tf'

    # features in list
    train_dataset = []
    test_dataset = []
    header = []

    def __init__(self, user, word_representation = 'tf', expand = False):
        """Prepare the dataset"""

        self.train_set = []
        self.test_set = []
        self.instances = [] # each instance is a tuple (featureset, label)                                                                                                                    
        self.dictionary = []
        self.dictionary_tf = {}
        self.pos_corpus = [] # list of words in positive tweets (for tf purposes)                                                                                                             
        self.neg_corpus = []
        self.train_set = []
        self.test_set = []
        self.word_representation = 'tf'

        self.train_dataset = []
        self.test_dataset = []
        self.header = []        

        self.corpus = [] # corpus in the LSA space

        self.train_rows = []
        self.test_rows = []
        
        logger.info("Init dataset for " + user)
        self.word_representation = word_representation

        rows = self.loaddataset(user, expand)
        self.rows = rows # for LDA use

        print "NEW DATASET: rows ", len(rows)
        # split train set and the use only train
        random.shuffle(rows)
        size = len(rows)
        # TODO: a mas palabras cojo, peor! penalizo palabras negativas, no penalizarlas
        cut = int(size*0.75)
        self.train_rows = rows[:cut]
        self.test_rows = rows[cut:]
        
        # get the global set of words of this dataset (TR)
        self.dictionary = self.extractdictionary(self.train_rows)

        # create corpus with pos and neg words (TR)                           
        (self.pos_corpus, self.neg_corpus) = self.initcorpus(self.train_rows)
        
        # get the global set of words of this dataset and its global tf ratio vector (TR)
        self.dictionary_tf = self.dictionaryTF(self.dictionary, self.pos_corpus, self.neg_corpus)
        
        # Compute LSA space for the training dataset 
        if (word_representation == 'lsa'): self.corpus = self.extractLSAcorpus()

        # create instances and fill their features (TR+TE)
        for row in self.train_rows:
            (features, tag) = self.row2instance(row)
            self.train_set.append((features, tag))
                        
        for row in self.test_rows:
            (features, tag) = self.row2instance(row)
            self.test_set.append((features, tag))            

        # Prepare classic dataset for libraries that need it
        (train, test) = self.dict2normal()
        header = self.dict2header()

        self.train_dataset = train
        self.test_dataset = test
        self.header = header

    def dict2normal(self):
        """ 
        Convert dictionary instances to list instances (more usable by Weka) 
        ({value_a:1, value_b:2}, 1) ---> (1,2,1)
        """
        
        train_set = self.train_set #dict
        test_set = self.test_set
        train_arrays = []
        test_arrays = []

        for tr in train_set:
            array = []
            for key, value in tr[0].items():
                array.append(value) # features
            array.append(tr[1]) # tag
            train_arrays.append(array)

        for te in test_set:
            array = [] 
            for key, value in te[0].items():
                array.append(value) # features
            array.append(te[1]) # tag
            test_arrays.append(array)
        return (train_arrays, test_arrays)

    def dict2header(self):
        """ 
        Create a header-list (usable by Weka) from the dictionary of features 
        ({value_a:1, value_b:2}, 1) ---> ("value_a", "value_b", "tag")
        """
    
        train_set = self.train_set
        header = []
        for key in train_set[0][0].keys():
            header.append(key)
        header.append('tag')
        return header

    # deprecated
    def split(self):
        """Split the dataset in training and test sets"""
        random.shuffle(self.instances)
        size = len(self.instances)
        cut = int(size*0.9)
        self.train_set = self.instances[:cut]
        self.test_set = self.instances[cut:]

    def row2instance(self, row):
        """Convert a row into a tagged instance with its features 
        (word features, trust, url)""" 

        # extract tag and features
        tag = self.gettag(row)
        hasurl = self.hasurl(row)
        trust = self.gettrust(row)
        words = self.preprocess(row.split('\t')[5])
        wfeatures = self.extractwordfeatures(words)

        # store features
        features = {}
        features["trust"] = trust
        features["url"] = hasurl
        features.update(wfeatures)

        return (features, tag)

    def extractLSAcorpus(self):
        """ 
        Cast wordfeatures to an LSA hypothesis space
        Returns a corpus in the LSA space
        """
        docs = []
        for row in self.rows:
            words = self.preprocess(row.split('\t')[5])
            doc = Document(words)
            docs.append(doc)
        corpus = Corpus(docs)
        print "NUMBER OF DOCS ", len(corpus.documents)
        #corpus.reduce(dimensions='TOP50')
        corpus.reduce()
        print "Corpus created and reduced"

        #print "NUMBER OF DOCS ", len(corpus.documents)
        #print "NUMBER OF DOCS LSA ", len(corpus.lsa.corpus.documents)
        #print "NUMBER OF TERMS ", len(corpus.lsa.terms)
        #print "NUMBER OF CONCEPTS ", len(corpus.lsa.concepts)

        # Para cada row, cast a "contains: word" o "lda: word"
        return corpus

    def extractwordfeatures(self, words):
        """ Extract features from words"""

        features = {}
        if (self.word_representation == 'bow'):
            features = self.extractbow(words)
        if (self.word_representation == 'tf'):
            dist = self.distance2corpus(words, self.dictionary_tf)
            features['similarity'] = dist #TODO y esto donde lo uso?
        if (self.word_representation == 'tf_expanded'):
            features = self.extracttf(words)
        if (self.word_representation == 'lsa'):
            features = self.corpus.lsa.transform(Document(words))
            
        return features

    def initcorpus(self, rows):
        """ Returns a positive and negative corpus of words"""
        pos_corpus = []
        neg_corpus = []
        for row in rows:
            tag = self.gettag(row)
            words = self.preprocess(row.split('\t')[5])

            if tag == 1:
                pos_corpus.extend(words)
            else:
                neg_corpus.extend(words)
        print "POS corpus of words ", len(pos_corpus)
        print "NEG corpus of words", len(neg_corpus)
        return (pos_corpus, neg_corpus)

    def distance2corpus(self, words, dictionary_tf):
        """ Compute cosine similarity with tf of corpus"""
        sim = 0
        for w in words:
            found = False
            for keyword, value in dictionary_tf.items():
                if w == keyword.split('(')[1][:-1]:
                    # don't penalize negative words (user didn't say they are bad!)
                    if (value > 0):
                        sim = sim + value
                        break
                    if (value < 0): # optional: to penalize negative words (this does not affect very much the accuracy (1% more aprox from 49% to 50%))
                        sin = sim + value
                        break
        return sim

    def extractdictionary(self, rows):
        """Returns the set of words from user (to create a dictionary of features)"""
        print "Call extract dictionary"

        wfeatures = []
        for row in rows:
            words = self.preprocess(row.split('\t')[5])
            wfeatures.extend(words)
        return set(wfeatures)

    def stem(self, words):
        porter = nltk.PorterStemmer()
        snowball = nltk.SnowballStemmer("spanish")
        snowball = nltk.SnowballStemmer("english")
        words = [snowball.stem(w) for w in words]
        return words

    def filterwords(self, words):
        """Delete short words as punctuation marks, numbers, isolated characters"""
        long_words = long_words = [w for w in words if len(w) > 1 and w is not '...' and w is not 'rt']
        return long_words

    def gettag(self, instance):
        """ Get tag (pos: +1; neg:-1) of the instance"""
        return int(instance.split('\t')[0])

    def hasurl(self, instance):
        return 'http' in instance

    def gettrust(self, instance):
        """Get trust associated to the tweet in the instance"""
        trust = instance.split('\t')[7]
        if float(trust) == -1: # not computed
            trust = trust # TODO ponerlo a 0
        return float(trust)

    def preprocess(self, text):
        """Extract a bag of words from tweet in an instance"""
        text = self.cleanuptext(text)
        words = self.text2words(text)
        words = self.filterwords(words) #delete short words  
        words = self.stopwords(words)
        words = self.stem(words)
        return words

    def extractbow(self, words):
        bow = {}
        #print "Dict words", words
        for keyword in self.dictionary:
            bow['contains(%s)' % keyword] = (keyword in words)
        return bow

    def extracttf(self, words):
        """ 
        Generates a bag of words weighted by its tf value
        (0,0,0,2.5,0,0,-1.6,...) Values are word_in_tweet * tf. 0 means the word is not in the tweet. tf is tfratio in the whole document.
        """

        tf = self.dictionary_tf.copy()
        for keyword, value in self.dictionary_tf.items():
            tf[keyword] = 0
            for w in words:
                if w == keyword.split('(')[1][:-1]:
                    tf[keyword] = value
                    break
        return tf

    def dictionaryTF(self, dictionary, pos_corpus, neg_corpus):
        """Extract a bag of words with TF(pos)/TF(neg) ratio from tweet in an instance"""
        dictionary_tf = {}
        for keyword in dictionary:
            # how many times in pos and neg examples
            pos = 0
            neg = 0
            if keyword in pos_corpus:
                pos = pos+1                
            if keyword in neg_corpus:
                neg = neg+1

            ratio = math.log((pos+1)/float(neg+1))
            dictionary_tf['tfratio(%s)' % keyword] = ratio

        return dictionary_tf

    def cleanuptext(self, text):
        """Cleanup text from urls and mentions"""
        re_mention = re.compile(r'@\w+')
        re_url = re.compile(r'http://[\w./?=&#+\-]+')
        text = re_mention.sub('', text)
        text = re_url.sub('', text)
        return text

    def text2words(self, text):
        """Chunk-tokenize the text in words"""
        #words = re.compile("\s").split(text)
        tokenizer = RegexpTokenizer('\w+|\$[\d\.]+|\S+')
        tokenizer = RegexpTokenizer('[a-zA-Z]+')
        words = tokenizer.tokenize(text)
        #words = word_tokenize(words)
        return words
        
    def stopwords(self, words):
        """Delete stopwords (spanish, english, catalan)"""
        stopspanish = stopwords.words('spanish')
        stopenglish = stopwords.words('english')
        words = [w for w in words if w.lower().encode("utf-8")  not in stopspanish]
        words = [w for w in words if w.lower().encode("utf-8") not in stopenglish]        
        return words

    def loaddataset(self, user, expand = False):
        """Load raw instances from user examples file"""
        wdir = globals.DATASETS_DIR
        if expand is False:
            filename = user + '.dataset'
        if expand is True:
            filename = user + '.expdataset'
        f = codecs.open(basepath() + wdir + filename, encoding='utf-8', mode='r')
        lines = f.readlines()
        f.close()
        return lines


class Classifier():
    """ Set of models to learn from dataset """
    
    classifier = None
    train_set = []
    test_set = []
    
    def __init__(self, dataset):
        """Prepare train and test sets"""
        self.dataset = dataset
        self.train_set = dataset.train_set
        self.test_set = dataset.test_set
        self.classifier = None

    def train(self):
        pass

    def predict(self, features):
        pass

    def test(self):
        pass

    def cv(self):
        pass

    def roc(self):
        pass


class NBClassifier(Classifier):
    """ Set of models to learn from dataset """

    def __init__(self, dataset):
        """Prepare train and test sets"""
        Classifier.__init__(self, dataset)

    def train(self, train_set):
        """ Train the classifier on training instances and return the trained classifier"""
        self.classifier = nltk.NaiveBayesClassifier.train(train_set)
        return self.classifier

    def test(self, test_set):
        """ Train and test a Naive Bayes model """
        if self.classifier is None:
            print "Classifier has not been trained"
            return
    
        accuracy =  nltk.classify.accuracy(self.classifier, test_set)
        print "Accuracy: " + str(accuracy)
        # print self.classifier.show_most_informative_features(5)
        return accuracy 

    def cv(self, train, K):
        train = self.train_set
        random.shuffle(train)
        total_accuracy = 0

        self.classifier = nltk.NaiveBayesClassifier.train(train) # para disponibilidad del confusion matrix
        for k in xrange(K):
            # Prepare samples
            tr = [x for i, x in enumerate(train) if i % K != k]
            va = [x for i, x in enumerate(train) if i % K == k]

            # Create model
            classifier = nltk.NaiveBayesClassifier.train(tr)
            accuracy =  nltk.classify.accuracy(classifier, va)
            total_accuracy += accuracy 
        
        # Get accuracy
        avg_accuracy = float(total_accuracy)/K
        print "CV Accuracy for Naive Bayes: " + str(avg_accuracy)
        return avg_accuracy

    

    def classify(self, featureset):
        """Classify a new example"""
        y = self.classifier.classify(featureset)
        return y

    def confusionmatrix(self):
        """ Get a confusion matrix for current dataset and classifier"""
        preds = []
        reals = []
        # get predicted-real table                                                
        for instance in self.test_set:
            pred = self.classifier.classify(instance[0])
            preds.append(pred)
            reals.append(instance[1])

        cm = nltk.ConfusionMatrix(reals, preds)
        print cm.pp(sort_by_count=True, show_percents=True, truncate=9)



class SVMClassifier(Classifier):
    """ Set of models to learn from dataset """
    
    def __init__(self, dataset):
        """Prepare train and test sets"""
        Classifier.__init__(self, dataset)

    def train(self, train_set):

        # Adapt instance to have something of the form:                                                                                                                         
        # [[-1, 1], [0, 0], [1, 1]], [-1, 0, -1] (tr, te)                                                                                                                       
        # clf.fit([[-1, 1], [0, 0], [1, 1]], [-1, 0, -1])                                                                                                                           

        # Cast features (booleans and ints) to floats                                                                                                                             
        for tr in train_set:
            for key, value in tr[0].items():
                tr[0][key] = float(value)
        
        tr_features = [tr[0].values() for tr in train_set]
        tr_tags = [tr[1] for tr in train_set]


        # svc = svm.SVC(kernel='rbf')                                                                                                                                             
        clf = svm.SVC()
        clf.fit(tr_features, tr_tags)
        self.classifier = clf
        print clf

    def predict(self, features):
        assert(self.classifier is not None)
        y = self.classifier.predict(features)
        return y

    def test(self, test_set):
        """ Get accuracy in test set """
        logger.info("Init Train-Test SVM")
        if self.classifier is None:
            print "Classifier has not been trained"
            return
        
        # Adapt instance to have something of the form:
        # [[-1, 1], [0, 0], [1, 1]], [-1, 0, -1] (tr, te)
        # clf.fit([[-1, 1], [0, 0], [1, 1]], [-1, 0, -1])
        
        # Cast features (booleans and ints) to floats
        for te in test_set:
            for key, value in te[0].items():
                te[0][key] = float(value)
        
        te_features = [te[0].values() for te in test_set]
        te_tags = [te[1] for te in test_set]
        
        # get accuracy SVM
        accuracy = 0
        self.preds = []
        self.gold = []
        for i in range(len(te_features)):
            pred = self.predict(te_features[i])
            if pred == te_tags[i]:
                accuracy = accuracy + 1
            self.preds.append(pred)
            self.gold.append(te_tags[i])
        
        accuracy = accuracy*100.0/len(test_set)
        print "SVM Accuracy", accuracy
        return accuracy


    def cv(self, train, K):
        """K-fold cross-validation over a train set"""
        
        train = self.train_set
        test = self.test_set
        header = self.dataset.header
        
        # Adapt instance to have something of the form:
        # [[-1, 1], [0, 0], [1, 1]], [-1, 0, -1] (tr, te)
        # clf.fit([[-1, 1], [0, 0], [1, 1]], [-1, 0, -1])
        
        # Cast features (booleans and ints) to floats
        for tr in train:
            for key, value in tr[0].items(): 
                tr[0][key] = float(value)
        for te in test:
            for key, value in tr[0].items():
                tr[0][key] = float(value)
        
        # Cross Validate
        random.shuffle(train)
        total_accuracy = 0
        for k in xrange(K):
            # Prepare samples
            training = [x for i, x in enumerate(train) if i % K != k]
            validation = [x for i, x in enumerate(train) if i % K == k]
            tr_features = [x[0].values() for x in training]
            tr_tags = [x[1] for x in training]
            te_features = [x[0].values() for x in validation]
            te_tags = [x[1] for x in validation]
            
            # Create model
            clf = svm.SVC()
            clf.fit(tr_features, tr_tags)
            
            # Get accuracy
            accuracy = 0 
            for i in range(len(validation)):
                pred = clf.predict(te_features[i])
                if pred == te_tags[i]:
                    accuracy +=1 
            
            accuracy = accuracy*100.0/len(validation)
            total_accuracy += accuracy
        
        avg_accuracy = float(total_accuracy)/K
        print "SVM Accuracy on CV: " + str(avg_accuracy)    
        return avg_accuracy


class SVMRankClassifier(Classifier):
    """ Set of models to learn from dataset """
    
    MAPcurves = []
    
    def __init__(self, dataset):
        """Prepare train and test sets"""
        Classifier.__init__(self, dataset)
        self.MAPcurves = []

    def train(self, train_set):
        # Adapt instance to have something of the form:                                                                                                                         
        # [[-1, 1], [0, 0], [1, 1]], [-1, 0, -1] (tr, te)                                                                                                                       
        # clf.fit([[-1, 1], [0, 0], [1, 1]], [-1, 0, -1])                                                                                                                           
        
        # Cast features (booleans and ints) to floats                                                                                                                             
        for tr in train_set:
            for key, value in tr[0].items():
                tr[0][key] = float(value)
        
        tr_features = [tr[0].values() for tr in train_set]
        tr_tags = [tr[1] for tr in train_set]
        
        
        # svc = svm.SVC(kernel='rbf')                                                                                                                                             
        clf = svm.SVR()
        clf.fit(tr_features, tr_tags)
        self.classifier = clf
        print clf

    def predict(self, features):
        y = self.classifier.predict(features)[0]
        return y

    def test(self, test_set):
        logger.info("Init Train-Test rankSVM")
    
        test = self.test_set
        header = self.dataset.header
    
        # Adapt instance to have something of the form:
        # [[-1, 1], [0, 0], [1, 1]], [-1, 0, -1] (tr, te)
        # clf.fit([[-1, 1], [0, 0], [1, 1]], [-1, 0, -1])
    
        # Cast features (booleans and ints) to floats
        for te in test:
            for key, value in te[0].items():
                te[0][key] = float(value)
    
        te_features = [te[0].values() for te in test]
        te_tags = [te[1] for te in test]
    
        # Get predictions
        list = []
        for row in dataset.test_rows:
            (features, tag) = dataset.row2instance(row) # to be able to access to the tweet text
            for key, value in features.items():
                features[key] = float(value)
            pred = self.predict(features.values())
            list.append((row.split('\t')[5], tag, pred))
    
        # Sort by prediction
        ranked = sorted(list, key=itemgetter(2))
        ranked.reverse()
        #pprint(ranked)
        return ranked
    
    def ROC(self, ranked):
        # Precision curve
        TPrate = []
        FPrate = []
        TP = 0  
        FP = 0
    
        P = 0
        N = 0
        for r in ranked:
            if r[1]>0: P +=1
            else: N +=1
    
        for r in ranked:
            if r[1] == 1: # True positive
                TP +=1
            if r[1] == -1: # False positive
                FP +=1
            TPrate.append(round(float(TP)/(P),4)) # que proporcion de pos tengo del total de la colección. Acabará siendo 100% si cojo toda la colección
            FPrate.append(round(float(FP)/(N),4)) # que proporcion de negs tengo del total de la colección. Acabará siendo 100% si cojo toda la colección.
    
        plt.plot(FPrate, TPrate, linewidth=1.0)
        plt.plot([0, 1], [0, 1], 'k--') #baseline
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.0])
        plt.xlabel('FPrate')
        plt.ylabel('TPrate)')
        plt.title('ROC curve of SVMrank')
        plt.grid(True)
        plt.show()
        
        return list


    def precision(self, ranked):
        """ Get a precission vector """
        precision = []
        i = 1
        TP = 0
        FP = 0
        for r in ranked:
            if r[1] == 1: # True positive (relevant)
                TP +=1
            i +=1
            precision.append(round(float(TP)/i,4))

        self.MAPcurves.append(precision)

#        plt.plot(numpy.arange(i-1), precision, linewidth=1.0)
#        plt.plot([0.0, i],[0.5, 0.5], 'k--') #baseline
#        plt.xlim([0.0, i])
#        plt.ylim([0.0, 1.0])
#        plt.xlabel('Items')
#        plt.ylabel('Precision)')
#        plt.title('Precision curve of SVMrank')
#        plt.grid(True)
#        plt.show()

        return precision

    def MAP(self):
        """ Get a MAP curve over all users. Up to 15 elements"""
        pprint(self.MAPcurves)
        MAP = [0]*11
        l = len(self.MAPcurves)
        
        for i in range(len(MAP)):
            for curve in self.MAPcurves:
                MAP[i] += float(curve[i])/l
    
        print "MAP:\n", MAP
        plt.plot(numpy.arange(len(MAP)), MAP, linewidth=1.0)
        plt.plot([0.0, i],[0.5, 0.5], 'k--') #baseline
        plt.xlim([0.0, i])
        plt.ylim([0.0, 1.0])
        plt.xlabel('Items')
        plt.ylabel('MAP)')
        plt.title('MAP curve of SVMrank')
        plt.grid(True)
        plt.show()
                
        return MAP
            

if __name__ == '__main__':

    total_accuracy = 0
    prec_curves = []
    ROC_curves = []

    file = open("conf/users_seed.conf", 'r')
    users = file.readlines()
    for user in users:
        
        # Test usera
        #user = 'test'
        #user = "ictlogist"
        #user = "alberto_lm"
        user = user.split()[0]
        print "***********************************************************"
        print "User: ", user
        # Create dataset                                                      
        dataset = Dataset(user,  word_representation = 'tf', expand = False)
        print "Train set size: ", len(dataset.train_set)
        print "Test set size: ", len(dataset.test_set)
        
        print "FEATURES size: ", len(dataset.test_set[0][0])
                
        # Choose classifier
        # classifier = NBClassifier(dataset)
        # classifier = SVMClassifier(dataset)
        classifier = SVMRankClassifier(dataset)
        
        # Train
        classifier.train(dataset.train_set)
        
        # Test
        #accuracy = classifier.test(dataset.test_set)
        #accuracy = classifier.cv(dataset.train_set, 5)
        
        ranked = classifier.test(dataset.test_set)
        ROC_curve = classifier.ROC(ranked)
        prec_curve = classifier.precision(ranked)
        ROC_curves.append(ROC_curve)
        prec_curves.append(prec_curve)
        
        # Get confusion matrix
        # classifier.confusionmatrix() # Solo va para NB
        continue
        total_accuracy += accuracy

    
    # DRAW MAP
    MAP = [0]*11
    l = len(prec_curves)
        
    for i in range(len(MAP)):
        for curve in prec_curves:
            MAP[i] += float(curve[i])/l
        
    print "MAP:\n", MAP
    plt.plot(numpy.arange(1,len(MAP)+1), MAP, linewidth=1.0)
    plt.plot([1.0, i],[0.5, 0.5], 'k--') #baseline
    plt.xlim([1.0, i])
    plt.ylim([0.0, 1.0])
    plt.xlabel('Items')
    plt.ylabel('MAP)')
    plt.title('MAP curve of SVMrank')
    plt.grid(True)
    plt.show()


    # Average accuracy of users
    avg_accuracy = float(total_accuracy)/len(users)
    print "****AVERAGE ACCURACY OVER ALL USERS", avg_accuracy
    logger.info("Average accuracy over all users: " + str(avg_accuracy))

#########################################
    # Recordar: Nltk no trabaja con numeros, se cree que 0 es una clase y 0.1 otra,

    #TODO: mirar frecuencia de palabras
    #TODO: mirar palabras relevantes
    
