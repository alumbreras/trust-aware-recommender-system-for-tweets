# -*- coding: utf-8 -*-

import logging
import logging.config
logging.config.fileConfig('./conf/logging.conf')
logger = logging.getLogger("dataset")

from os.path import *
import difflib

import globals
import os
import utils
import codecs
import tweetexpander

def generate_dataset(expand = False):
    """ Generates a dataset with positive and negative examples """
    logger.info("Generating datasets")
    
    wdir_timelines = globals.TIMELINES_DIR
    wdir_datasets = globals.DATASETS_DIR
    basepath = utils.basepath()

    # Get the seed users
    seed_file = utils.basepath() + globals.USERS_SEED_FILE
    seed_users = utils.get_users_from_file(seed_file)
    
    for user in seed_users:
        logger.info("Generating dataset of user: " + str(user))
        #user = "hibakusha"
        filepath = wdir_timelines + user + ".timeline"
        if not os.path.exists(filepath): continue

        # Get the pos and neg examples for the user)
        (positives, negatives) = get_examples(user) # TODO esto no en utils sino en este mismo modulo.

        # Create dataset with positive/negative instances. Expanded tweets if necessary
        if expand is False:
            f = codecs.open(basepath + wdir_datasets + user + ".dataset", encoding='utf-8', mode='w+')
        if expand is True:
            f = codecs.open(basepath + wdir_datasets + user + ".expdataset", encoding='utf-8', mode='w+')

        for pos in positives:
            instance = makeinstance(pos, 1, expanded=expand)
            if instance is None: continue
            f.write("+1" + "\t" + instance)
        for neg in negatives:
            instance = makeinstance(neg, -1, expanded=expand)
            if instance is None: continue
            f.write("-1" + "\t" + instance) # TypeError: cannot concatenate 'str' and 'NoneType' objects
        f.close()
 

def makeinstance(instance, tag, expanded=False):
    
    # Get original instance
    try:
        type = instance.split('\t')[0]
        user = instance.split('\t')[1]
        statusid = instance.split('\t')[2]
        date = instance.split('\t')[3]
        text = instance.split('\t')[4] + " " # makes url filter easier
        source = instance.split('\t')[5]
        trust = instance.split('\t')[6]
    except IndexError as e:
        logger.exception("Error while processing instance:\n" + instance + "\n" + str(e))
        return None
    
    if expanded == True:
        print repr(text)
        logger.info("Expanding tweet:\n" + text)
        
        # Clean out urls and usernames to have a wider search
        cleaned = utils.tweetclean(text)
        logger.debug("Cleaned tweet:\n" + cleaned)
        
        # Expand tweet
        expanded = tweetexpander.expandtweet(cleaned)
        if expanded != None:
            #text = text.encode("utf-8") + ' ' + expanded.encode("utf-8")
            text = text + ' ' + expanded
            logger.info("Expanded tweet:\n" + text)
        else:
            logger.warning("Tweet not expanded")
    #text = unicode(text, "utf-8")
    print repr(text)
    # text = text.encode("utf-8")

    # Instance
    instance = \
        type + '\t' + \
        user + '\t' + \
        statusid + '\t' + \
        date + '\t' + \
        text + '\t' + \
        source + '\t' + \
        trust
    
    return instance

def get_examples(user):
    """Get positive and negative examples for user.
       Positive examples are RTs
    """
    
    pos = []
    neg = []
    
    # read user.timeline 
    wdir = globals.TIMELINES_DIR
    filename = user + '.timeline'
    #f = open(utils.basepath() + wdir + filename, 'r')
    f = codecs.open(utils.basepath() + wdir + filename, 'r', 'utf-8')
    statuses = f.readlines()
    f.close()
    
    # Look for retweets (positive example)
    length = len(statuses)
    count = 0
    found = 0
    for status in statuses:
        count +=1
        logger.info("Processing user statuses: " + str(count) + "/" + str(length))
        logger.info("Found: " + str(found))
        type = status.split('\t')[0]
        
        if len(status.split('\t')) != 7: continue
        text = status.split('\t')[4]
        
        # print repr(text)
                
        if type ==  "retweet":
            
            #find the original tweet
            # print "Finding ", status
            src_user = None
            try:
                src_user = status.split('\t')[5]
            except IndexError as exception:
                print exception
                print "WARNING: bad stored status:\n", status
                continue
            
            orig_status = find_tweet(src_user, status)
            if orig_status == None: continue
            else: found +=1
        
            # add as a positive example                                                                                               
            # print "POS", orig_status
            pos.append(orig_status.decode("utf-8"))
            
            # add previus to status as a negative example
            position = get_tweet_pos(src_user, orig_status)
            if position < 1: continue
            prev_pos = position - 1
            prev_status = get_tweet(src_user, prev_pos)
            # print "NEG.", prev_status
            neg.append(prev_status.decode("utf-8"))
    
    return (pos,neg)

def find_tweet(user, status):
    """Find the most similar status"""
    
    wdir = globals.TIMELINES_DIR
    filename = user + '.timeline'
    
    if not exists(utils.basepath() + wdir + filename):
        print "user not crawled (", user, ")"
        return None
    
    f = open(utils.basepath() + wdir + filename, 'r')
    
    statuses = f.readlines()
    f.close()
    match = difflib.get_close_matches(status, statuses, 1, 0.5)
    
    if len(match) == 0:
        print "match no found"
        return None
    
    print "Match found"
    
    return match[0]
    
def get_tweet_pos(user, status):
    """Get the position of the more similar tweet within user tweets""" 
    
    wdir = globals.TIMELINES_DIR
    filename = user + '.timeline'
    f = open(utils.basepath() + wdir + filename, 'r')
    statuses = f.readlines()
    
    (match,) = difflib.get_close_matches(status, statuses, 1, 0.5)
    if match == None: 
        print "match no found"
        return None
    
    print "Found match!:\n", match
    
    # find line of match
    pos = 0
    for line in statuses:
        if match in line:
            break        
        pos += 1
    
    print "Position", pos
    return pos

def get_tweet(user, pos):
    """Get full information of a tweet in a given position"""
    
    wdir = globals.TIMELINES_DIR
    filename = user + '.timeline'
    f = open(utils.basepath() + wdir + filename, 'r')
    statuses = f.readlines()
    return statuses[pos]

if __name__ == '__main__':
    generate_dataset(expand=True)

