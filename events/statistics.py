import globals
import utils
import subprocess
import os
import sys
from subprocess import Popen


def retweet_rate(user=None):

    print "***retweet_rate***\n"
    basepath = utils.basepath()
    wdir = basepath + globals.TIMELINES_DIR

    rt = 0
    total = 0

    if (user != None):
        filepath = wdir + user + ".timeline"
        if not os.path.exists(filepath): 
            print "Sorry, user not found"
            return
        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["grep", "retweet"], stdin = p1.stdout, stdout=subprocess.PIPE)
        p3 = Popen(["wc", "-l"], stdin=p2.stdout, stdout=subprocess.PIPE)
        user_retweets = int(p3.communicate()[0].split()[0])

        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["wc", "-l"], stdin=p1.stdout, stdout=subprocess.PIPE)
        user_total = int(p2.communicate()[0].split()[0])

        rt += user_retweets
        total += user_total

        rate = float(rt)/total
        print "Total retweet rate:", rate
        return rate

    # Get the users                                                                                                       
    seed_file = globals.USERS_SEED_FILE
    disc_file = globals.USERS_DISCOVERED_FILE1
    seed_users = utils.get_users_from_file(seed_file)
    disc_users = utils.get_users_from_file(disc_file)

    print "\n*seed users:\n"
    for user in seed_users:

        filepath = wdir + user + ".timeline"
        if not os.path.exists(filepath): 
            continue
        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["grep", "retweet"], stdin = p1.stdout, stdout=subprocess.PIPE)
        p3 = Popen(["wc", "-l"], stdin=p2.stdout, stdout=subprocess.PIPE)
        user_retweets = int(p3.communicate()[0].split()[0])
        
        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["wc", "-l"], stdin=p1.stdout, stdout=subprocess.PIPE)
        user_total = int(p2.communicate()[0].split()[0])        
        
        rt += user_retweets
        total += user_total
        
        print user, float(user_retweets)/user_total
            
    print "\n*discovered users (distance 1):\n"
    for user in disc_users:
                
        filepath = wdir + user + ".timeline"
        if not os.path.exists(filepath):
            continue
        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["grep", "retweet"], stdin = p1.stdout, stdout=subprocess.PIPE)
        p3 = Popen(["wc", "-l"], stdin=p2.stdout, stdout=subprocess.PIPE)
        user_retweets = int(p3.communicate()[0].split()[0])

        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["wc", "-l"], stdin=p1.stdout, stdout=subprocess.PIPE)
        user_total = int(p2.communicate()[0].split()[0])
            
        rt += user_retweets
        total += user_total

        print user, float(user_retweets)/user_total

    rate = float(rt)/float(total)
    return rate

def retweet_availability(user=None):

    wdir_datasets = utils.basepath() + globals.DATASETS_DIR
    wdir_timelines = utils.basepath() + globals.TIMELINES_DIR
    seed_file = utils.basepath() + globals.USERS_SEED_FILE
    
    seed_users = utils.get_users_from_file(seed_file)
    
    examples = 0
    total = 0

    print "\n*retweet_availability:\n"
    for user in seed_users:

        filepath = wdir_timelines + user + ".timeline"
        if not os.path.exists(filepath): continue
        
        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["grep", "retweet"], stdin = p1.stdout, stdout=subprocess.PIPE)
        p3 = Popen(["wc", "-l"], stdin=p2.stdout, stdout=subprocess.PIPE)
        user_retweets = int(p3.communicate()[0].split()[0])
        total += user_retweets

        filepath = wdir_datasets + user + ".dataset" 
        p1 = Popen(["cat", filepath], stdout=subprocess.PIPE)
        p2 = Popen(["wc", "-l"], stdin=p1.stdout, stdout=subprocess.PIPE)
        user_examples = int(p2.communicate()[0].split()[0])
        examples += user_examples
        
        user_rate = 0.5*float(user_examples)/user_retweets # user_examples counts positive and negative.
        
        print user, user_rate

    rate = 0.5*float(examples)/total 

    return rate

if __name__ == "__main__":
    
    if (len(sys.argv)) == 2:
        rate = retweet_rate(sys.argv[1])

    if (len(sys.argv)) == 1:
        rate = retweet_rate()

    print "\nRetweet rate:", rate
    
    rate = retweet_availability()
    print "\nRetweet availavility rate:", rate
