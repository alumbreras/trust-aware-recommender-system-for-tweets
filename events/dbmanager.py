import logging
from sqlite3 import dbapi2 as sqlite
from decimal import Decimal
import os
import globals 
from collections import Counter

logger = logging.getLogger("dbmanager")
logger.setLevel(logging.DEBUG)

class DBmanager():

    def __init__(self, dbname = globals.TRUSTDB_FILE):
    
        #Create index tables if not exists
        logger.debug("db init")
        if not os.path.exists(dbname):
            self.con=sqlite.connect(dbname)
            self.createindextables()
        else:
            self.con=sqlite.connect(dbname)


    def __del__(self):
        self.con.close()
        
    def dbclose(self):
        self.con.close()

    def dbcommit(self):
        self.con.commit()
        
    def dbdelete(self):
        if os.path.exists(dbname):
            os.remove(dbname)

    def createindextables(self):
        self.con.execute('create table users(userid)')
        self.con.execute('create table mentions(fromid, toid, count)')
        self.con.execute('create table retweets(fromid, toid, count)')
        self.con.execute('create table favorites(fromid, toid, count)')
        self.con.execute('create index useridx on users(userid)')
        self.con.execute('create index mentionsidx on mentions(fromid, toid)')
        self.con.execute('create index retweetsidx on retweets(fromid, toid)')
        self.con.execute('create index favoritesidx on favorites(fromid, toid)')
        self.dbcommit()

    def delete_user(self, user):
        self.con.execute("DELETE FROM mentions WHERE toid=?", (user,))
        self.con.execute("DELETE FROM retweets WHERE toid=?", (user,))
        self.con.execute("DELETE FROM favorites WHERE toid=?", (user,))
        

    def addmention(self, fromid, toid, decay=0):
        """Fromid mentioned from toid"""

        logger.debug(str(fromid) + "mentioned" + str(toid))
        # Apply decay factor
        if (decay>0):
            self.decay(fromid, "mentions", decay)


        # Add mention
        u = self.con.execute("SELECT rowid FROM mentions WHERE fromid=? AND toid=?" 
                             , (fromid, toid)).fetchone()

        if u == None:
            self.con.execute("INSERT INTO mentions(fromid, toid, count) VALUES (?,?,?)" 
                             , (fromid, toid, 0))

        self.con.execute("UPDATE mentions SET count = count + 1 WHERE fromid=? AND toid=?" 
                         , (fromid, toid))

        self.dbcommit()


    def addfavorite(self, fromid, toid, decay=0):
        """Fromid favorited from toid"""

        # Apply decay factor
        if (decay>0):
            self.decay(fromid, "favorites", decay)

        # Add mention
        u = self.con.execute("SELECT rowid FROM favorites WHERE fromid=? AND toid=?"
                             , (fromid,toid)).fetchone()

        if u == None:
            self.con.execute("INSERT INTO favorites(fromid, toid, count) VALUES (?,?,?)"
                             , (fromid, toid, 0))

        self.con.execute("UPDATE favorites SET count = count + 1 WHERE fromid=? AND toid=?"
                         , (fromid, toid))

        self.dbcommit()

    def addretweet(self, fromid, toid, decay=0):
        """Fromid retweeted from toid"""
        logger.debug(str(fromid) + "retweeted" + str(toid))

        # Apply decay factor
        if (decay>0):
            self.decay(fromid, "retweets", decay)


        # Add mention
        u = self.con.execute("SELECT rowid FROM retweets WHERE fromid=? AND toid=?"
                             , (fromid,toid)).fetchone()

        if u == None:
            self.con.execute("INSERT INTO retweets(fromid, toid, count) VALUES (?,?,?)"
                             , (fromid, toid, 0))

        self.con.execute("UPDATE retweets SET count = count + 1 WHERE fromid=? AND toid=?"
                         , (fromid, toid))

        self.dbcommit()

    def decay(self, fromid, matrix, decay):
        """Apply a forgetting factor to all the user's interaction"""
        
        factor = 1-decay

        if matrix is "mentions":
            self.con.execute("UPDATE mentions SET count = count*? WHERE fromid=?"
                             , (factor, fromid))

        if matrix is "retweets":
            self.con.execute("UPDATE retweets SET count = count*? WHERE fromid=?"
                             , (factor, fromid))
        
        if matrix is "favorites":
            self.con.execute("UPDATE favorites SET count = count*? WHERE fromid=?"
                             , (factor, fromid))

        self.dbcommit()

    def neighbors(self, user):
        """ Returns a list of tuples (user, count) that have been RT/mentioned/faved by user"""

        RT_nodes = self.con.execute("SELECT DISTINCT toid, count FROM retweets WHERE fromid=?"
                                       , (user,)).fetchall()
        mention_nodes = self.con.execute("SELECT DISTINCT toid, count FROM mentions WHERE fromid=?"
                                       , (user,)).fetchall()
        fav_nodes = self.con.execute("SELECT DISTINCT toid, count FROM favorites WHERE fromid=?"
                                       , (user,)).fetchall()

        count = Counter()
        for (user, strength) in RT_nodes:
            count[user] += strength
        for (user, strength) in mention_nodes:
            count[user] += strength
        for (user, strength) in fav_nodes:
            count[user] += strength

        return count



    def dump_matrix_to_database(self, users, matrix):
        """ Dump a numpy matrix into the database """
        # TODO. Probably this functions is not necessary 

        # Reset old table
        self.con.execute("DROP TABLE IF EXISTS npinteractions")
        self.con.execute("CREATE TABLE npinteractions(fromid, toid, count)")
        
        # Fill the table
        for u in users:
            row = trust(u) # get the trust from u to every user
            for v in users:
                self.con.execute("INSERT INTO npinteractions(fromid, toid, count) VALUES (?,?,?)"
                                 , (user, row[0], row[1]))


    def transitions_db_to_matrix(self):
        """ Transform the transition table into a transition matrix"""

        # Get users
        # get all users
        users = self.con.execute("SELECT fromid FROM mentions UNION SELECT fromid FROM retweets UNION SELECT fromid FROM favorites").fetchall()
        users = [user for (user,) in users]

        # Create index of users. Necessary because matrix does not tag rows with users
        self.users = users

        matrix = []
        i = 0
#        print users
        print "Number of users:", len(users)
        for u in users:
            i += 1
            print str(i) + "/" + str(len(users))
            list = []
            for v in users:
                count = self.con.execute("SELECT count FROM transitions WHERE fromid=? AND toid=?"
                                       , (u, v)).fetchone()
                if count is None: count = 0
                else: count = count[0]
                list.append(count)
            matrix.append(list)
            
        return matrix


    def get_users(self):
        return self.users

    def init_trust_matrix(self):
        """Get initial trust matrix (transition matrix)"""
        
        self.con.execute("DROP TABLE IF EXISTS transitions")
        self.con.execute("CREATE TABLE transitions(fromid, toid, count)")
        self.con.execute("CREATE index transitionsidx on transitions(fromid, toid)")
        self.dbcommit()

        # get all users       
        users = self.con.execute("SELECT fromid FROM mentions UNION SELECT fromid FROM retweets UNION SELECT fromid FROM favorites").fetchall()
    
        for userid in users:
            userid = userid[0]
            # find users mentioned by userid
            trustedlist = self.con.execute("SELECT DISTINCT toid, count FROM mentions WHERE fromid=?"
                                       , (userid,)).fetchall()

            # update trust matrix
            for trusted in trustedlist:
                u = self.con.execute("SELECT count FROM transitions WHERE fromid=? AND toid=?"
                                     , (userid, trusted[0])).fetchone()

                if u is None:

                    self.con.execute("INSERT INTO transitions(fromid, toid, count) VALUES (?,?,?)"
                                     , (userid, trusted[0], trusted[1]))
                else:

                    self.con.execute("UPDATE transitions SET count=count+? WHERE fromid=? AND toid=?"
                                 , (trusted[1], userid, trusted[0]))
    
            # find users retweeted from userid
            trustedlist = self.con.execute("SELECT DISTINCT toid, count FROM retweets WHERE fromid=?"
                                           , (userid,)).fetchall()
            # update trust matrix
            for trusted in trustedlist:
                u = self.con.execute("SELECT count FROM transitions WHERE fromid=? AND toid=?"
                                 , (userid, trusted[0])).fetchone()
                if u is None:
                    self.con.execute("INSERT INTO transitions(fromid, toid, count) VALUES (?,?,?)"
                                     , (userid, trusted[0], trusted[1]))
                else:
                    self.con.execute("UPDATE transitions SET count=count+? WHERE fromid=? AND toid=?"
                                 , (trusted[1], userid, trusted[0]))

                self.dbcommit()

            # find users favorited from userid
            trustedlist = self.con.execute("SELECT DISTINCT toid, count FROM favorites WHERE fromid=?"
                                           , (userid,)).fetchall()

            # update trust matrix
            for trusted in trustedlist:
                u = self.con.execute("SELECT count FROM transitions WHERE fromid=? AND toid=?"
                                 , (userid, trusted[0])).fetchone()
                if u is None:
                    self.con.execute("INSERT INTO transitions(fromid, toid, count) VALUES (?,?,?)"
                                     , (userid, trusted[0], trusted[1]))
                else:
                    self.con.execute("UPDATE transitions SET count=count+? WHERE fromid=? AND toid=?"
                                     , (trusted[1], userid, trusted[0]))
                self.dbcommit()

        # Add autoreference arcs to allow paths with real length < N (include dead-end nodes)
        users = self.con.execute("SELECT DISTINCT fromid FROM transitions UNION SELECT DISTINCT toid FROM transitions").fetchall()
        for user in users:
            fromid=user[0]
            self.con.execute("INSERT INTO transitions(fromid, toid, count) VALUES(?,?,?)", (fromid, fromid, 1))

        self.normalize_matrix('transitions')
        self.dbcommit()


    def normalize_matrix(self,table):
       
        if table is "mentions":
            logger.debug("Normalizing " + str(table))
            users = self.con.execute("SELECT DISTINCT fromid FROM mentions").fetchall()
            for user in users:
                fromid = user[0]
                dests = self.con.execute("SELECT toid, count FROM mentions WHERE fromid=?", (fromid,)).fetchall()
                totalcount = 0
                for dest in dests:
                    totalcount = totalcount + dest[1]
                self.con.execute("UPDATE mentions SET count=count/? WHERE fromid=?", (float(totalcount), fromid))

        if table is "retweets":
            logger.debug("Normalizing " + str(table))
            users = self.con.execute("SELECT DISTINCT fromid FROM retweets").fetchall()
            for user in users:
                fromid = user[0]
                dests = self.con.execute("SELECT toid, count FROM retweets WHERE fromid=?", (fromid,)).fetchall()
                totalcount = 0
                for dest in dests:
                    totalcount = totalcount + dest[1]
                self.con.execute("UPDATE retweets SET count=count/? WHERE fromid=?", (float(totalcount), fromid))
        
        if table is "favorites":
            logger.debug("Normalizing " + str(table))
            users = self.con.execute("SELECT DISTINCT fromid FROM favorites").fetchall()
            for user in users:
                fromid = user[0]
                dests = self.con.execute("SELECT toid, count FROM favorites WHERE fromid=?", (fromid,)).fetchall()
                totalcount = 0
                for dest in dests:
                    totalcount = totalcount + dest[1]
                self.con.execute("UPDATE favorites SET count=count/? WHERE fromid=?", (float(totalcount), fromid))

        if table is "transitions":
            logger.debug("Normalizing " + str(table))
            users = self.con.execute("SELECT DISTINCT fromid FROM transitions").fetchall()
            for user in users:
                fromid = user[0]
                dests = self.con.execute("SELECT toid, count FROM transitions WHERE fromid=?", (fromid,)).fetchall()
                totalcount = 0
                for dest in dests:
                    totalcount = totalcount + dest[1]
                self.con.execute("UPDATE transitions SET count=count/? WHERE fromid=?", (float(totalcount), fromid))

        self.dbcommit()
 

    def trust_matrix_pow(self, pow):
        """ multiply the matrix by pow+1 to calculate the final trust matrix
        and store the result in 'trust' table"""
        
        if len(self.con.execute("PRAGMA table_info(transitions)").fetchall()) == 0:
            self.init_trust_matrix()
        
        # Make a permanent copy of initial trust matrix (transition matrix)
        self.con.execute("DROP TABLE IF EXISTS temp")    
        self.con.execute("CREATE TABLE temp(fromid, toid, count DEFAULT 0)")
        self.con.execute("CREATE index tempidx on temp(fromid, toid)")
        self.con.execute("INSERT INTO temp (fromid,toid,count) SELECT * FROM transitions")

        # Make a copy of trust table trust->newtrust
        self.con.execute("DROP TABLE IF EXISTS newtemp")
        self.con.execute("CREATE TABLE newtemp(fromid, toid, count DEFAULT 0)")
        self.con.execute("CREATE index newtempidx on newtemp(fromid, toid)")
        self.dbcommit()

        multiplications = 0
        queries = 0
        for iter in range(pow-1):

            # print "\n******Iter******", iter
            # Multiply trust matrix by its trasposed (nm = na*am + nb*bm + nc*cm... )
            #na is taken from the current matrix. am is taken from the origina transition matrix
            # Get origin users
            users = self.con.execute("SELECT DISTINCT fromid FROM temp").fetchall()
            queries += 1
            for user in users:
                usera = user[0]

                # Get middle users
                midusers = self.con.execute("SELECT toid, count FROM temp WHERE fromid=?", (usera,))
                queries += 1
                if midusers is None:
                    break

                for midusertuple in midusers.fetchall():
                    miduser = midusertuple[0]
                    trusta = midusertuple[1]
                    #Use trust table instead of transition if we want to square the current trust matrix
                    # Get end users
                    destusers = self.con.execute("SELECT toid, count FROM transitions WHERE fromid=?", (miduser,))
                    queries += 1
                    if destusers is None:
                        break

                    for destusertuple in destusers.fetchall():
                        userb = destusertuple[0]
                        trustb = destusertuple[1]
                  
                        # Multiply a-b trust and b-c trust to get trust of this path
                        if (trusta == None or trustb == None): trust = 0
                        else: 
                            trust = trusta*trustb
                            multiplications += 1

                        # Get last trust
                        u = self.con.execute("SELECT fromid, toid, count FROM newtemp WHERE fromid=? AND toid=?"
                                             , (usera, userb)).fetchone()
                        # Update trust
                        if u is None:
                            self.con.execute("INSERT INTO newtemp(fromid,toid,count) VALUES (?,?,?)"
                                             , (usera, userb, trust))
                        else:
                            self.con.execute("UPDATE newtemp SET count=count+? WHERE fromid=? AND toid=?"
                                             , (trust, u[0], u[1]))
                        
                        self.dbcommit()

            logger.info("db queries: " + str(queries))
            logger.info("multiplications: " + str(multiplications))
        
            
            # (iteration drump)Dump newtrust table into trust table newtrust->trust
            self.con.execute("DROP TABLE IF EXISTS temp")
            self.con.execute("CREATE TABLE temp(fromid, toid, count)")
            self.con.execute("CREATE index tempidx on temp(fromid, toid)")
            self.con.execute("INSERT INTO temp (fromid,toid,count) SELECT * FROM newtemp")
            self.con.execute("DROP TABLE IF EXISTS newtemp")
            self.con.execute("CREATE TABLE newtemp(fromid, toid, count DEFAULT 0)")
            self.con.execute("CREATE index newtempidx on newtemp(fromid, toid)")            
            self.dbcommit()

        #self.print_trust_matrix('temp')
        self.dbcommit()
                        
    def calculate_trust_matrix(self, maxlength=3):
        """Calculate final trust matrix considering paths up to maxlength
        and store the result in 'trust' table"""

        # Initalize transition matrix from mentions, retweets and favorites
        #TODO: is it necessary?
        self.init_trust_matrix()        

        logger.info("Calculating trust matrix")
        # Get alphas and constant (to make the c*a1 + c*a2 + ... = 1)
        sum = 0
        for a in range(0,maxlength):
            sum = sum + 1.0/pow(2,a)
        c = 1.0/sum
        alphas = []
        for a in range(0,maxlength):
            alphas.append(c*1.0/pow(2,a))

        logger.info("alphas computed. Preparing tables")

        #print alphas
        # Calculate matrices aplha1*A, alpha2*AA, alpha3+AAA
        self.con.execute("DROP TABLE IF EXISTS trust")
        self.con.execute("CREATE TABLE trust(fromid, toid, count DEFAULT 0)")
        self.con.execute("CREATE index trustidx on trust(fromid, toid)")

        for pathlength in range(1,maxlength+1):
            logger.info("Computing pow(A," + str(pathlength) + ")")
            self.trust_matrix_pow(pathlength)

            #sum this matrix to the acumulated sum
            arcs = self.con.execute("SELECT fromid, toid, count FROM temp").fetchall()
            for arc in arcs:

                weightedcount = alphas[pathlength-1]*arc[2]
                u = self.con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?",
                                 (arc[0], arc[1])).fetchone()
                if u is None:
                    self.con.execute("INSERT INTO trust(fromid,toid,count) VALUES (?,?,?)"
                                     , (arc[0], arc[1], weightedcount))
                else:
                    self.con.execute("UPDATE trust SET count=count+? WHERE fromid=? AND toid=?"
                                     , (weightedcount, arc[0], arc[1]))
                self.dbcommit()

        self.dbcommit() #not necessary
        
    def gettrust(self, fromid, toid):
        """Get how much fromid trusts on toid"""

        if len(self.con.execute("PRAGMA table_info(trust)").fetchall()) == 0:
            self.calculate_trust_matrix()

        trust = self.con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                 , (fromid, toid)).fetchone()

        if trust is not None:
            return trust[0]
        else:
            return 0

    def get_top_influencers(self, user, ntop):
        """Get the three user with more influence over user"""

        if len(self.con.execute("PRAGMA table_info(trust)").fetchall()) == 0:
            self.calculate_trust_matrix()

        top = self.con.execute("SELECT toid, count FROM trust WHERE fromid=? ORDER BY count DESC", (user,)).fetchall()
        logger.debug("Top influencers of" + str(user) + ":\n" + str(top))
        return top[0:ntop]


    def print_matrix(self, table):

        #u = self.con.execute("SELECT fromid, toid, count FROM ? ORDER BY fromid", (table,)).fetchall()
        u = self.con.execute("SELECT fromid, toid, count FROM trust ORDER BY fromid").fetchall() 
        print "\nFinal trust matrix"
        for row in u:
            print row[0], "-" ,row[1], "=" ,row[2]


    def set_trust_matrix(self, trust_matrix):
        """ Stores a trust matrix (numpy matrix) into trust table"""

        # Reset trust table
        self.con.execute("DROP TABLE IF EXISTS trust")
        self.con.execute("CREATE TABLE trust(fromid, toid, count)")

        # Fill table
        users = self.users
        i = 0
        for u in users:
            j = 0
            for v in users:
                self.con.execute("INSERT INTO trust(fromid,toid,count) VALUES (?,?,?)"
                                 , (u, v, trust_matrix[i,j]))
                j +=1 # next column (to)
            i+=1 # next row (from)

        self.dbcommit()
