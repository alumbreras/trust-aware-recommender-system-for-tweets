from os.path import *
import globals
import difflib

# can try with leventshein distance or SequenceMatcher class in difflib

def basepath():
    """Get the basepath of the project"""

    basepath = abspath(dirname(__file__)) + '/../'
    return basepath

def get_users_from_file(filename):
    """Get a list of users from a file that has a user per line"""

    #TODO: do I really need the [:-1]
    users = []
    # read users    
    if exists(filename):
        file = open(filename, 'r')
        lines = file.readlines()
        file.close()
        for line in lines:
            users.append(line[:-1].split('\t')[0]) #lines include \n

    return users


def set_users_to_file(filename, users, fromuser=None):
    """Set a list of users to a file that has a user per line
    "user" argument (optional) is the user associated to users"""

    file = open(filename, 'w')
    if fromuser is not None:
        for user in users:
            file.write(str(user) + '\t' + str(fromuser) + '\n')
    else:
        for user in users:
            file.write(str(user) + '\n')

    file.close()

def tweetclean(text):

    text = urldelete(text)
    text = usernamedelete(text)
    text = RTdelete(text)
    text = hashtagdelete(text)
    return text

def urldelete(text):
    """"Delete urls from tweet"""
    pos = text.find("http://")
    if pos == -1:
        return text # clean text
    ta = text[:text.find("http://")]
    tb = text[text.find("http://"):]
    tb = tb[tb.find(" "):]
    result = ta + tb
    return urldelete(result)

def usernamedelete(text):
    """Delete '@usernames:' from tweets"""

    pos = text.find("@")
    if pos != -1: # unclean text
        ta = text[:text.find("@")]
        tb = text[text.find("@"):]
        tb = tb[tb.find(" "):]
        text = ta + tb
        return usernamedelete(text)
    else: #clean text
        return text


def RTdelete(text):
    """ Delete 'RT ' from tweets"""
    pos = text.find("RT ")
    if pos != -1: # unclean text
        ta = text[:text.find("RT ")]
        tb = text[text.find("RT "):]
        tb = tb[tb.find(" "):]
        text = ta + tb
        return RTdelete(text)
    else: #clean text
        return text

def hashtagdelete(text):
    """ Delete '#hashtags ' from tweets"""

    pos = text.find("#")
    if pos != -1: # unclean text                               
        ta = text[:text.find("#")]
        tb = text[text.find("#"):]
        tb = tb[tb.find(" "):]
        text = ta + tb
        return hashtagdelete(text)
    else: #clean text                                                  
        return text
