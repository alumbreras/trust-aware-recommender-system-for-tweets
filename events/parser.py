import logging

logger = logging.getLogger("parser")

def parse_tweet(status):
    """Parse a status object"""

    info = {'type':None,
            'user_mentioned':None,
            'user_retweeted':None,
            'user_favorited':None}
    
    status_type = 'normal'
    RT = []
    mentioned = []
    favorited = []
    logger.debug("Parsing status:" + status.text)
    if ("RT @" in status.text): #dont consider RT@
        status_type = 'retweet'
        logger.debug(str(status.entities['user_mentions']))
        if  len(status.entities['user_mentions']) > 0:
            RT.append(status.entities['user_mentions'][0]['screen_name'])
            logger.debug("is a RT @")

    elif (" via @" in status.text or " (via @" in status.text or "/via @" in status.text):
        status_type = 'retweet'
        if  len(status.entities['user_mentions']) > 0:
            size = len(status.entities['user_mentions'])
            RT.append(status.entities['user_mentions'][size-1]['screen_name'])
            logger.debug("is a via @")

    elif len(status.entities['user_mentions'])>0:
        status_type = 'mention'
        for mention in status.entities['user_mentions']:
            mentioned.append(mention['screen_name'])
        logger.debug("is a mention")


    # protect RT from RT with @ and no user mentions (bad constructed RT)
    if  status_type == 'retweet' and len(RT) is 0:
        logger.warning(" Bad formed retweet: " + status.text)
        status_type = 'normal'

    info['type'] = status_type
    info['user_mentioned'] = mentioned
    info['user_retweeted'] = RT
    info['user_favorited'] = favorited

    return info

def get_interactions(timeline):
    """Get interactions from a timeline object"""
    interactions = {'mentioned': None,
                    'retweeted': None,
                    'favorited': None}

    mentioned = []
    retweeted = []
    favorited = []

    for status in timeline:
        info = parse_tweet(status)
        if info['type'] == 'mention':
            mentioned.append(info['user_mentioned'][0])
            logger.debug(status.user.screen_name + "mentioned " + str(info['user_mentioned']))
        if info['type'] == 'retweet':
            retweeted.append(info['user_retweeted'][0])
            logger.debug(status.user.screen_name + "retweeted" + str(info['user_retweeted']))
        if info['type'] == 'favorite':
            favorited.append(info['user_favorited'][0])
            logger.debug(status.user.screen_name + "favorited " + str(info['user_favorited']))

    interactions['mentioned'] = mentioned
    interactions['retweeted'] = retweeted
    interactions['favorited'] = favorited

    return interactions
