import logging
import globals
import os
import utils
from os.path import *
import dbmanager

logger = logging.getLogger("usersmanager")

def is_user(user):
    """Return true if user is getting recommendations"""

    filename = globals.USERS_SEED_FILE
    if exists(filename):
        f = open(filename, 'r')
        text = f.read()
        if text.find(user + '\n') is not -1:
            return True
        f.close()

    filename = globals.USERS_FILE
    if exists(filename):
        f = open(filename, 'r')
        lines = f.read()
        if text.find(user + '\n') is not -1:
            return True
    f.close()
    return False

def get_users(seed=True, events=True, discovered1=True, discovered2=True):
    """Return users in the system"""

    users = []

    # read users
    if (events):
        filename = join(utils.basepath(), globals.USERS_FILE)
        userslist = utils.get_users_from_file(filename)
        for u in userslist:
            users.append(u[:-1])

    if (seed):
        filename = join(utils.basepath(), globals.USERS_SEED_FILE)
        userslist = utils.get_users_from_file(filename)
        for u in userslist:
            users.append(u)


    if (discovered1):
        filename = join(utils.basepath(), globals.USERS_DISCOVERED_FILE1)
        if exists(filename):
            userslist = utils.get_users_from_file(filename)
            for u in userslist:
                users.append(u)


    if (discovered2):
        filename = join(utils.basepath(), globals.USERS_DISCOVERED_FILE2)
        userslist = utils.get_users_from_file(filename)
        for u in userslist:
            users.append(u)

    return users


def delete_user(user):
    """Delete a user from user files and from trust matrix"""
    
        #TODO: borra todos los usuarios.                                                                                          
    filename = globals.USERS_SEED_FILE
    if exists(filename):
        f = open(filename, 'r')
        lines = f.readlines()
        i = 0
        for line in lines:
            if user in line:
                del lines[i]
            i = i +1
        f.close()
        f = open(filename, 'w')
        f.writelines(lines)
        f.close()

    filename = globals.USERS_DISCOVERED_FILE1
    if exists(filename):
        f = open(filename, 'r')
        lines = f.readlines()
        i = 0
        for line in lines:
            if user in line:
                del lines[i]
            i = i +1
        f.close()
        f = open(filename, 'w')
        f.writelines(lines)
        f.close()

    filename = globals.USERS_DISCOVERED_FILE2
    if exists(filename):
        f = open(filename, 'r')
        lines = f.readlines()
        i = 0
        for line in lines:
            if user in line:
                del lines[i]
            i = i +1
        f.close()
        f = open(filename, 'w')
        f.writelines(lines)
        f.close()

    filename = globals.USERS_FILE
    if exists(filename):
        f = open(globals.USERS_FILE, 'r')
        lines = f.readlines()
        i = 0
        for line in lines:
            if user in line:
                del lines[i]
            i = i +1
        f.close()
        f = open(globals.USERS_FILE, 'w')
        f.writelines(lines)
        f.close()

    db = dbmanager.DBmanager(globals.TRUSTDB_FILE)
    db.delete_user(user)


def update_top_influentials(users, outputfile, ntop):

    db = dbmanager.DBmanager(globals.TRUSTDB_FILE)

    # Find most influential that are not in the "users" input list
    outputusers = []
    for user in users:
        logger.info("get_top_influencers")
        top = db.get_top_influencers(user, ntop)
        toplist = [x[0] for x in top]
        track_top_influencials_changes(user, toplist)
        logger.debug("Top influentials of " + str(user) + ":\n" + str(top))
        for candidate in top:
            if candidate[0] not in outputusers and candidate[0] not in users:
                outputusers.append([candidate[0], user])

    # Delete old output file
    if os.path.exists(outputfile):
        os.remove(outputfile)

    # Add user if not seed user
    file = open(outputfile, 'w')
    for candidate in outputusers:
        if not is_user(candidate[0]):
            file.write(str(candidate[0]) + "\t" + str(candidate[1]) + "\n")
    file.close()

    db.dbclose()

def track_top_influencials_changes(user, top):
    logger.info("executing: track_top_influentials_changles")
    filename = join(utils.basepath(), globals.TOPTRUST_DIR + user + ".toptrust")
    oldusers = utils.get_users_from_file(filename)
    rpin
    utils.set_users_to_file(filename, top, fromuser=user)
    logger.debug("old users: " + str(oldusers))
    logger.debug("new users: " + str(top))
    # Compare old and new
    match = 0
    total = 0
    for new in top:
        if new in oldusers:
            match = match + 1
        total = total + 1
    new = total - match

    # Update number of changes in a file for the user
    filename = join(utils.basepath(), globals.TOPTRUST_DIR + user + ".rate")
    f = open(filename, 'a')
    f.write(str(new) + "," + str(top) + '\n')
    f.close()
    logger.info("influencers changed for " + user + ":" + str(new) )

def get_user_throughput(user, span=3):
    #average throughput overa round
    pass

def update_users():
    """Add the to the discovered lists 3 most influential users of every users"""

    ntop = globals.NTOP

    # Update the 3 most influential of seed users        
    seedusers = get_users(seed=True, events=True, discovered1=False, discovered2=False)
    print "Seedusers", seedusers
    update_top_influentials(seedusers, globals.USERS_DISCOVERED_FILE1, ntop)

    # Update the 3 most influential of discovered1 users
    discusers1 = get_users(seed=False, events=False, discovered1=True, discovered2=False)
    print "Disc1users", discusers1
    update_top_influentials(discusers1, globals.USERS_DISCOVERED_FILE2, ntop)


