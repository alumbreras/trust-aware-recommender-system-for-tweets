import json
import urllib
from BeautifulSoup import BeautifulSoup
import mechanize
from pybing.query import WebQuery
from pybing import Bing
import authentication 

def expandtweet(tweet, searchengine='bing'):
  expanded = tweet
  if searchengine == 'google':
    expanded = googleexpand(tweet)

  if searchengine == 'bing':
    expanded = bingexpand(tweet)

  return expanded

def googleexpand(searchfor):
  query = urllib.urlencode({'q': searchfor.encode("utf-8"), 'rsz': 8})
  url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
  search_response = urllib.urlopen(url)
  search_results = search_response.read()
  results = json.loads(search_results)
  print results
  data = results['responseData']
  hits = data['results']
  
  if len(hits) == 0:
    return None
  
  print 'Total results: %s' % data['cursor']['estimatedResultCount']
  print 'Top %d hits:' % len(hits)
  #for h in hits: print ' ', h['url']
  #print 'For more results, see %s' % data['cursor']['moreResultsUrl']

  expanded = " " 
 
  for hit in hits:
    print hit['content']
    content = hit['content']
    soup = BeautifulSoup(content)
    r = soup.getText(separator = ' ')
    expanded = expanded + ' ' +  r
  return expanded

def bingexpand(searchfor):
  """ Returns and expansion for the term using Bing search results"""
  expanded = ' '
  bing = Bing('503CBBF37DEBE941B17A65FE754203CB5106CA1B')
  response = bing.search_web(searchfor.encode("utf-8"))
  #print response['SearchResponse']['Web']['Total']
  try:
    print response
    results = response['SearchResponse']['Web']['Results']
  except KeyError as e:
    print "**********No results found for this query"
    return expanded
    
  size = min(len(results), 200)
  print "results/200 minsize: ", size
  if size == 0: 
    return expanded
  print len(results)
  for result in results[:size]:
    try: 
      expanded = expanded + ' ' +  result['Description']
      print result['Description']
    except KeyError as e:
      print "**********No description found in this entry"


  return expanded

 
def twitterexpand(searchfor):
  """ Returns and expansion for the term using Bing search results"""
  expanded = ' ' 
  #TODO identificar nombres y buscar solo eso
  # apis nltk POS tagging
  text = nltk.word_tokenize(searchfor)
  pos = nltk.pos_tag(text)
  #http://nltk.googlecode.com/svn/trunk/nltk_data/index.xml
  #Lluis marquez ha colaborado con corpus de catalan
  
  api = authentication.get_api()
  results = api.search(searhfor)
  size = len(results)
  for result in results[:size]:
    try:
      expanded = expanded + ' ' +  result.text
      print result.text
    except IndexError as e:
      print e
    
  return expanded



   
def bingexpand_deprecated(searchfor):

  br = mechanize.Browser()
  br.set_handle_robots(False)
  br.open("http://www.bing.com/search?count=1&q=cheese")
  content = br.response()
  content = content.read()
  print content

  #result['SearchResponse']
  soup = BeautifulSoup(content, convertEntities=BeautifulSoup.ALL_ENTITIES)
  print "***** RESULTADO *******"
  #print soup
  return soup

if __name__ == "__main__":
  expandtweet("Catalunya planea trocear la sanidad publica para abrirla al capital privado")
    
