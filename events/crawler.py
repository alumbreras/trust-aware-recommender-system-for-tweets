import logging
import logging.config
logging.config.fileConfig('./conf/logging.conf')

import time
import threading
import os
from os.path import *

from tweepy.error import TweepError

import globals
import authentication
import dbmanager
import parser
import utils
import SNmanager
import usersmanager
import tweetslogger
import trust

logger = logging.getLogger("crawler")

class Crawler(threading.Thread):

     def __init__(self, interval, api):
        threading.Thread.__init__(self)
        self.interval = interval
        self.api = api
        self.finished = threading.Event()
        self.rounds = 0
 
     def run(self):
          
          while not self.finished.is_set():
               
               users = usersmanager.get_users()
               logger.debug("Current users:" + str(users))          
               logger.info("Current users being tracked: " + str(len(users)))

               if (True): # for tests, False if want to go directly to matrix computation
                    for user in users:

                         # Get timeline updates
                         logger.info("Tracking user " + str(user))
                         self.rounds += 1
                         timeline = self.get_user_timeline(user)

                         if timeline is None: 
                              logger.warning("No timeline from " + user)
                              continue 

                         elif len(timeline) > 0:
                              tweetslogger.log_user_timeline(user, timeline)
                         else:
                              logger.info('No new tweets for ' + user)

                         # Update interactions
                         interactions = parser.get_interactions(timeline)
                         mentioned = interactions['mentioned']
                         retweeted = interactions['retweeted']
                         favorited = interactions['favorited']
                         SNmanager.update_trust(user, mentioned, retweeted, favorited, decay=globals.DECAY)
                         logger.info("Mentions:" + str(len(mentioned)))
                         logger.info("Retweets:" + str(len(retweeted)))
                         logger.debug("Favorites:" + str(len(favorited)))
                         logger.info("API COUNTER - users rounds:" + str(self.rounds))

               # Recalculate trust and update user list
               db = dbmanager.DBmanager(globals.TRUSTDB_FILE)
               logger.info("Deleting irrelevant users...")
               SNmanager.delete_irrelevant_users()
               logger.info("Calculating trust matrix...")
               # db.calculate_trust_matrix() # old way. multiply matrix in db
               trust.calculate_trust() # new way. multiply matrix in memory
               db.dbclose()
               usersmanager.update_users()

               logger.info("Going to sleep...")

               i = 0
               while not self.finished.is_set() and i < self.interval:
                    self.finished.wait(60) #sleep 1 minute
                    remaining = self.interval - i
                    print remaining, " minutes to wake up..."
                    i = i+1
               

     def cancel(self):
          self.finished.set()

     def get_user_timeline(self, user):

          since_id = tweetslogger.get_last_status_id(user)
          try:
               timeline = self.api.user_timeline(user, include_rts=True, include_entities=True, since_id=since_id)
          except TweepError as e:
               logger.exception("User:", str(user))
               print "Exception", e
               logger.exception("Exception: ", str(e))
               logger.exception("Error when requesting timeline of user " + str(user))
               logger.exception("Status: " + str(e.response.status) + " Reason: " +  str(e.response.reason))
               status = e.response.status
               if status == 404:
                    usersmanager.delete_user(user)
                    logger.warning(" Deleted user: " + user)
               return
          except AttributeError as e:
               logger.exception("Exception: ", str(e))
          return timeline

def main():

    api = authentication.get_api()
    crawler = Crawler(globals.TRACKING_FREQUENCY, api)

    try:
        crawler.start()
    except KeyboardInterrupt:
        crawler.cancel()
        logger.exception("\Crawler interrupted")
        print "\Goodbye!"

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print '\nGoodbye!'
