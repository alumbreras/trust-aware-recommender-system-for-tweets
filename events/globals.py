# files
USERS_FILE = 'crawled/users/users.log' # deprecated
USERS_SEED_FILE = 'conf/users_seed.conf'
USERS_DISCOVERED_FILE1 = 'crawled/users/users_discovered_level1.log'
USERS_DISCOVERED_FILE2 = 'crawled/users/users_discovered_level2.log'
EVENTS_FILE = 'crawled/events/events.log' 
TRUSTDB_FILE = 'crawled/trust.db'
TIMELINES_DIR = 'crawled/timelines/'
TOPTRUST_DIR = 'crawled/toptrust/'
STREAM_FILE = 'crawled/stream.log'
DATASETS_DIR = 'datasets/'

# variables
NTOP = 5
#"SELECT toid, count FROM mentions WHERE count < TRUST_THRESHOLD?"
#TRUST_THRESHOLD = 0.05 #25%?? mas bien 5% (es sobre uno, pq el count se normaliza)
TRUST_THRESHOLD = 0.05 #TODO truncar mas en los mas alejados de los seed
#TODO: esto crece indefinidamente, como pararlo? coger los N de cada usuario. N=100, por ejemplo

DECAY = 0.05
TRACKING_FREQUENCY = 240 # minutes per loop
