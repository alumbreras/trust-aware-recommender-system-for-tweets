import logging
logger = logging.getLogger("tweetslogger")

import globals
import SNmanager

import os
from os.path import *
import parser
wdir = globals.TIMELINES_DIR


def log_user_timeline(user, timeline):

    timeline_file =  user + '.timeline'

    fa = open(wdir + timeline_file, 'a+')
    fb = open(globals.STREAM_FILE, 'a+')
    timeline.reverse() #older first                                                                                       

    for status in timeline:

        user_mentioned = "none"
        user_retweeted = "none"
        user_favorited = "none"

        info = parser.parse_tweet(status)
        if info['type'] is 'mention':
            user_mentioned = info['user_mentioned'][0]
        if info['type'] is 'retweet':
            user_retweeted = info['user_retweeted'][0]
        if info['type'] is 'favorite':
            user_favorited = info['user_favorited'][0]

        reference_user = "none"
        if user_retweeted != "none":
            reference_user = user_retweeted
        if user_mentioned != "none":
            reference_user = user_mentioned
        if user_favorited != "none":
            reference_user = user_favorited
        
        trust = -1
        if reference_user != "none":
            trust = SNmanager.get_trust(user, reference_user)
            
        output = str(info['type']) + '\t' \
                + str(status.user.screen_name) + '\t' \
                + str(status.id) + '\t' \
                + str(status.created_at) + '\t' \
                + status.text.replace('\r', ' ').encode('utf-8') + '\t' \
                + str(reference_user) + '\t' \
                + str(trust) \
                + '\n'

        fa.write(output)
        fb.write(output)

    fa.close()
    fb.close()
    set_last_status_id(user, str(status.id))

def get_last_status_id(user):

    laststatus_file = user + '.laststatus'

    if exists(wdir + laststatus_file):
        f = open(wdir + laststatus_file, 'r')
        since_id = f.readline()[:-1] #take out the \n
        f.close()
        logger.info("log user " + user + " since " + since_id)

    else:
        since_id = None

    return since_id
    
def set_last_status_id(user, statusid):
    
    laststatus_file = user + '.laststatus'
    logger.debug(" setting last status " + user + " " + statusid )
    f = open(wdir + laststatus_file, 'w')
    f.write(statusid + '\n')
    f.close()
