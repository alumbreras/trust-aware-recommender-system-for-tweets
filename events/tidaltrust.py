import numpy as np
from collections import defaultdict
import math

def adj(n):
    adjs = [i for i in range(len(rating[n])) if rating[n][i] > 0 and i is not n]
    return adjs

def tidal_trust(source, sink):

    global color
    global G
    global q
    global rating

    dim = len(G)    
    q.append(source)
    depth = 1
    maxdepth = 10000
    d = defaultdict(list) #store nodes at every depth                                                                                       
    path_flow = [-1]*dim #trust from source to a node
    cached_rating = np.zeros((dim, dim))
    cached_rating.fill(-1)
    children = defaultdict(list) # dict of stacks
    temp_q = []
    while len(q) > 0 and depth <= maxdepth:
        print "************Depth", depth
        print "************Remaining queue", q
        #raw_input()
        n = q.pop() # q: nodes at current depth
        d[depth].append(n)
        print "***Exploring node:", n
        print "adj(" + str(n) + "): " +  str(adj(n))
        # if neighbor has direct rating of the sink                                                                          
        if sink in adj(n):
            print "sink found!!!"
            cached_rating[n][sink] = rating[n][sink]
            maxdepth = depth
            flow = min(path_flow[n], rating[n][sink]) # get current path flow
            if path_flow[n] == -1: flow = rating[n][sink]
            print "sink ---" +  "through " + str(n) + ":" +  str(path_flow[n])
            print "Node rate of sink is ", rating[n][sink]
            print "path rate of node is", path_flow[n]
            print "Previous path_flow_sink (current max)", path_flow[sink]
            path_flow[sink] = max(path_flow[sink], flow) # gral. path flow               
            print "Updated path_flow_sink (updated max)", path_flow[sink]
            children[n].append(sink)
            
        # if neighbor has not direct rating of the sink                                                                         
        else: # explore the children
            print "-sink not reached..."
            for n2 in adj(n):
                if color[n2] is 'white': # not seen before
                    color[n2] = 'gray'
                    print "Color of " + str(n2) + " was white. Turned to gray" 
                    temp_q.append(n2) # add this node to queue
                    print "Added  " + str(n2) + " to temp_q"
                if n2 in temp_q: # if its first time n2 is found, children of n
                    print "previous path_flow to " + str(n2) + ": " + str(path_flow[n2])
                    flow = min(path_flow[n], rating[n][n2]) # get current path flow
                    if path_flow[n] == -1: flow = rating[n][n2]
                    path_flow[n2] = max(path_flow[n2], flow) # update gral. path flow
                    children[n].append(n2) #nodes not previously discovered
                    print "----updated path_flow to " + str(n2) + ": " + str(path_flow[n2])
                    print "current temp_q", temp_q

        # Update collected queue for next iteration of search
        if len(q) == 0:
            print "temp_q collected"
            print temp_q
            q = temp_q
            temp_q = []
            depth = depth + 1
            print "Path flows", path_flow
            print "Cycle finished. Next queue:", q
            
    maxthreshold = path_flow[sink]
    depth = depth - 1 # funciona si hago depth -2
    print "Children tree", children
    print "Depth map", d
    print "Depth", depth
    print "Max Threshold", maxthreshold
    print "Cached rating\n", cached_rating
    print "Path flows", path_flow

    raw_input() 
    # Go back to the source
    print "************** Go back to the source***********"
    numerator = 0
    denominator = 0
    while depth > 0:
        while len(d[depth]) > 0:
            print "d(depth)", d[depth]
            n = d[depth].pop()
            print "computing sink rating from", n
            print "childrens of node:", children[n]
            if sink not in children[n]: # or make initial depth = depth - 2
                for n2 in children[n]:
                    # if n2 is sink: continue # dont query sink about itself!!
                    print "rating of child", rating[n][n2]
                    print "rating of child of sink. cached_rating", n2, sink, cached_rating[n2][sink]
                    if (rating[n][n2]>=maxthreshold) and cached_rating[n2][sink] >= 0:
                        print "This child has a rating for he sink"
                        numerator += rating[n][n2] * cached_rating[n2][sink]
                        denominator += rating[n][n2]
                if denominator > 0:
                    cached_rating[n][sink] = float(numerator)/denominator
                else:
                    cached_rating[n][sink] = -1
                print "cached rating updated\n", cached_rating
        depth -= 1

    print "FINAL TRUST:", cached_rating[source,sink]
    return cached_rating[source,sink]
                

if __name__ == '__main__':
    
    rating = np.array([(0,9,1,0),
                       (5,0,5,0),
                       (9,9,0,9),
                       (0,0,0,0)])

    rating = np.array([(0,9,7,9),
                       (8,0,6,0),
                       (3,6,0,8),
                       (0,0,0,0)])

    rating = np.array([(9.0,9.1,9.3,0),
                       (8.0,8.9,8.0,8.5),
                       (7.0,6.9,0.0,8.0),
                       (8.0,8.9,8.0,8.0)])

    rating = np.array([(0, 9, 8, 10, 0, 0, 0),
                       (0, 0, 0,  0, 9, 8, 0),
                       (0, 0, 0, 0, 10, 10, 0),
                       (0, 0, 0, 0,  0,  9, 0),
                       (0, 0, 0, 0,  0, 0, 8),
                       (0, 0, 0, 0, 0, 0, 6),
                       (0, 0, 0, 0, 0, 0, 0)])
                       

    print rating

    G = range(7)
    color = {}
    for n in G:
        color[n] = 'white' # white - not visited

    q = []

    tidal_trust(0,6)
    
    markov_trust = rating + rating*rating + rating*rating*rating

    print markov_trust
