import time
from getpass import getpass
from textwrap import TextWrapper
import tweepy
import neareventsauth
from tweepy.streaming import StreamListener, Stream


class StreamWatcherListener(StreamListener):

    def __init__(self):
        super(StreamWatcherListener, self).__init__()
        self.status_wrapper = TextWrapper(width=80, initial_indent='    ', subsequent_indent='    ')

    def on_status(self, status):
      try:
        print '-' * 20
        print self.status_wrapper.fill(status.text)
        print '\n %s  %s \n' % (status.author.screen_name, status.created_at)

        # store the detected status
        file = open("events.log", 'a')
        file.write(str(status.author.screen_name) + "\t" + str(status.author.id) + "\t" + str(status.text) + "\n" )
        file.close()

        # read current users
        users = []
        file = open('users.log', 'r')
        lines = file.readlines()
        file.close()
        for line in lines:
            users.append(line[:-1])

        # register user if new:
        if(str(status.author.id) not in users):
            file = open('users.log', 'a')
            file.write(str(status.author.id) + "\n")
            file.close()
            
      except:
        print "Exception on_status"
        # Catch any unicode errors while printing to console
        # and just ignore them to avoid breaking application.
        pass
      
    def on_error(self, status_code):
      print 'An error has occured! Status code = %s' % status_code
      return False  # keep stream alive
    
    def on_timeout(self):
      print 'Snoozing Zzzzzz'


def main():
    
  auth = authentication.get_auth()
  listener = StreamWatcherListener()
  stream = tweepy.Stream(auth, listener, timeout=None)
  track_list =  ( "#addevent", )
  
  stream.filter( track=track_list, async=False )
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print '\nGoodbye!'

