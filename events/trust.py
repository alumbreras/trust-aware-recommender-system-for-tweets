import logging
from sqlite3 import dbapi2 as sqlite
from decimal import Decimal
import os
import globals 
import dbmanager
import usersmanager
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from collections import defaultdict
logger = logging.getLogger("trust")
logger.setLevel(logging.DEBUG)

def calculate_trust():

    # Get transitions matrix
    db = dbmanager.DBmanager()
    rating = db.transitions_db_to_matrix()
    rating = np.array(rating)
    markovTrust = MarkovTrust(rating)

    logger.info("Computing Markov Trust")
    trust_matrix = markovTrust.compute_trust()
    print trust_matrix[0][0]    

    # Store results in db
    db.set_trust_matrix(trust_matrix)

    #tidalTrust = TidalTrust(rating)
    #print "TIDAL TRUST"
    #trust_matrix = tidalTrust.compute_trust()
    #print trust_matrix

    #At the end, update "trust" table
    return trust_matrix

class Trust():
    def __init__(self):
        #db.interactions(usera,userb)
        pass
    def direct_trust(self):
        users = con.execute("SELECT fromid FROM mentions UNION SELECT fromid FROM retweets UNION SELECT fromid FROM favorites").fetchall()
        for user in users:
            userid = user[0]
            
        
    def trust(usera, userb):
        raise NotImplementedError

    def propagate_trust(trust_matrix):
        raise NotImplementedError


    def __private(self):
        pass


class MarkovTrust(Trust):
    def __init__(self, rating):
        Trust.__init__(self)
        self.rating = rating
        self.dim = len(self.rating[0])


    def compute_trust(self, maxlength=3):
        rating = np.mat(self.rating) # cast to numpy matrix to ease some computations
        dim = self.dim
        trust_matrix = np.zeros((dim, dim))

        # Normalize rating matrix to to make it probabilistic (sum of output trust = 1)
        for i in range(dim):
            sum = np.sum(rating[i])
            rating[i] = rating[i]/sum

        # Get alphas and constant (to make the c*a1 + c*a2 + ... = 1)
        # exponential decay with path length

        sum = 0
        for i in range(maxlength):
            sum = sum + 1.0/pow(2,i)
        c = 1.0/sum

        alphas = []
        for i in range(maxlength):
            alphas.append(c*1.0/pow(2,i))

        trust_matrix =  alphas[0]*rating + alphas[1]*(rating**2) + alphas[2]*(rating**3)

        # Re-Normalize rating matrix to to make it probabilistic (sum of output trust = 1)
        # just in case decimals denormalized it
        for i in range(dim):
            sum = np.sum(trust_matrix[i])
            trust_matrix[i] = trust_matrix[i]/sum

        print trust_matrix
        return trust_matrix
        
class TidalTrust(Trust):
    def __init__(self, rating):
        Trust.__init__(self)
    
#        self.rating = np.array([(0, 9, 8, 10, 0, 0, 0),
#                           (0, 0, 0,  0, 9, 8, 0),
#                           (0, 0, 0, 0, 10, 10, 0),
#                           (0, 0, 0, 0,  0,  9, 0),
#                           (0, 0, 0, 0,  0, 0, 8),
#                           (0, 0, 0, 0, 0, 0, 6),
#                           (0, 0, 0, 0, 0, 0, 0)])

        self.rating = rating
        self.dim = len(self.rating[0])
        

    def compute_trust(self):
        dim = self.dim
        trust_matrix =  np.zeros((dim, dim))
        for source in range(dim):
            for sink in range(dim):
                trust_matrix[source][sink] = self.tidal_trust(source, sink)

        return trust_matrix


    def adj(self,n):
        rating = self.rating
        adjs = [i for i in range(len(rating[n])) if rating[n][i] > 0 and i is not n]
        return adjs
        
    def tidal_trust(self, source, sink):
        rating = self.rating

        # init variables and stacks
        G = range(len(rating[0]))
        color = {}
        for n in G:
            color[n] = 'white' # white - not visited                                                                                 
        q = []

        # Tidal Trust
        dim = len(G)    
        q.append(source)
        depth = 1
        maxdepth = 1000
        d = defaultdict(list) #store nodes at every depth                                                                                       
        path_flow = [-1]*dim #trust from source to a node
        cached_rating = np.zeros((dim, dim))
        cached_rating.fill(-1)
        children = defaultdict(list) # dict of stacks
        temp_q = []
        while len(q) > 0 and depth <= maxdepth:
#            print "************Depth", depth
#            print "************Remaining queue", q
            #raw_input()
            n = q.pop() # q: nodes at current depth
            d[depth].append(n)
#            print "***Exploring node:", n
#            print "adj(" + str(n) + "): " +  str(self.adj(n))
            # if neighbor has direct rating of the sink                                                                          
            if sink in self.adj(n):
#                print "sink found!!!"
                cached_rating[n][sink] = rating[n][sink]
                maxdepth = depth
                flow = min(path_flow[n], rating[n][sink]) # get current path flow
                if path_flow[n] == -1: flow = rating[n][sink]
#                print "sink ---" +  "through " + str(n) + ":" +  str(path_flow[n])
#                print "Node rate of sink is ", rating[n][sink]
#                print "path rate of node is", path_flow[n]
#                print "Previous path_flow_sink (current max)", path_flow[sink]
                path_flow[sink] = max(path_flow[sink], flow) # gral. path flow               
#                print "Updated path_flow_sink (updated max)", path_flow[sink]
                children[n].append(sink)
            
            # if neighbor has not direct rating of the sink                                                                         
            else: # explore the children
#                print "-sink not reached..."
                for n2 in self.adj(n):
                    if color[n2] is 'white': # not seen before
                        color[n2] = 'gray'
#                        print "Color of " + str(n2) + " was white. Turned to gray" 
                        temp_q.append(n2) # add this node to queue
#                        print "Added  " + str(n2) + " to temp_q"
                    if n2 in temp_q: # if its first time n2 is found, children of n
#                        print "previous path_flow to " + str(n2) + ": " + str(path_flow[n2])
                        flow = min(path_flow[n], rating[n][n2]) # get current path flow
                        if path_flow[n] == -1: flow = rating[n][n2]
                        path_flow[n2] = max(path_flow[n2], flow) # update gral. path flow
                        children[n].append(n2) #nodes not previously discovered
#                        print "----updated path_flow to " + str(n2) + ": " + str(path_flow[n2])
#                        print "current temp_q", temp_q
            
            # Update collected queue for next iteration of search
            if len(q) == 0:
#                print "temp_q collected"
#                print temp_q
                q = temp_q
                temp_q = []
                depth = depth + 1
#                print "Path flows", path_flow
#                print "Cycle finished. Next queue:", q

        maxthreshold = path_flow[sink]
        depth = depth - 1 # funciona si hago depth -2
#        print "Children tree", children
#        print "Depth map", d
#        print "Depth", depth
#        print "Max Threshold", maxthreshold
#        print "Cached rating\n", cached_rating
#        print "Path flows", path_flow

        # Go back to the source
#        print "************** Go back to the source***********"
        numerator = 0
        denominator = 0
        while depth > 0:
            while len(d[depth]) > 0:
#                print "d(depth)", d[depth]
                n = d[depth].pop()
#                print "computing sink rating from", n
#                print "childrens of node:", children[n]
                if sink not in children[n]: # or make initial depth = depth - 2
                    for n2 in children[n]:
                        # if n2 is sink: continue # dont query sink about itself!!
#                        print "rating of child", rating[n][n2]
#                        print "rating of child of sink. cached_rating", n2, sink, cached_rating[n2][sink]
                        if (rating[n][n2]>=maxthreshold) and cached_rating[n2][sink] >= 0:
#                            print "This child has a rating for he sink"
                            numerator += rating[n][n2] * cached_rating[n2][sink]
                            denominator += rating[n][n2]
                    if denominator > 0:
                        cached_rating[n][sink] = float(numerator)/denominator
                    else:
                        cached_rating[n][sink] = -1
#                    print "cached rating updated\n", cached_rating
            depth -= 1

#        print "FINAL TRUST:", cached_rating[source,sink]
        return cached_rating[source,sink]






if __name__ == "__main__":

    rating = np.array([(0, 9, 8, 10, 0, 0, 0),
                       (0, 0, 0,  0, 9, 8, 0),
                       (0, 0, 0, 0, 10, 10, 0),
                       (0, 0, 0, 0,  0,  9, 0),
                       (0, 0, 0, 0,  0, 0, 8),
                       (0, 0, 0, 0, 0, 0, 6),
                       (0, 0, 0, 0, 0, 0, 0)])
    
    tidalTrust = TidalTrust(rating)
    trust_matrix = tidalTrust.compute_trust()
    print trust_matrix


    markovTrust = MarkovTrust(rating)
    trust_matrix = markovTrust.compute_trust()
    print trust_matrix


    calculate_trust()

    # Create the network matrix
#    matrix = db.database_to_matrix()
#    users = db.get_users()
#    matrix = numpy.array(matrix)

#    print matrix
#    print matrix**1
#    print matrix**2
#    print matrix**10
#    print matrix.size

#    D=nx.DiGraph(matrix)
#    nx.draw(D)
#    nx.draw_spectral(D)
#    plt.show()
