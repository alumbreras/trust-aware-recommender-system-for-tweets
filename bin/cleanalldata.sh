#!/bin/bash

rm $PWD/crawled/timelines/*
rm $PWD/crawled/users/users_discovered*
rm $PWD/crawled/toptrust/*
rm $PWD/crawled/trust.db
rm $PWD/crawled/stream.log

rm $PWD/log/* 