# -*- coding: utf-8 -*-

import unittest
from os.path import *
from events import tweetexpander

class TestUtils(unittest.TestCase):

    def __test_googleexpand(self):

        tweet_unicode = u"sanidad publica catalana"
        tweet_unicode = "alberto lumbreras"
        tweet_unicode = "CCOO denuncia el tancament de Pizza Grill a #Badalona http://t.co/ScoU2Sj "
        tweet_str = "sanidad pública catalana"
        tweet_utf8 = tweet_unicode.encode("utf-8")

        # Independiente del original
        # con encode(utf-8) en el resultado parce que funciona
        result = tweetexpander.googleexpand(tweet_unicode)
        print "\n\n\n\n*******Unicode query:\n", type(result)
        f1 = open("test_unicode", "w")
        f1.write("unicode: " + result.encode("utf-8"))
        f1.close()

        f1 = open("test_unicode", "w")
        line = f1.readline()
        f1.close()
        length = len(line)
        
        assert length > 3
        assert True

    def test_bingexpand(self):

        tweet = u"esdrújula"
        result = tweetexpander.bingexpand(tweet)
        print "\n\n\n\n*******Original tweet:\n ", tweet
        print "\n\nBing expanded result:\n", result
        string = tweet + result

        f1 = open("test_bing", "w")
        f1.write("unicode: " + string)
        f1.close()

        f1 = open("test_bing", "r")
        line = f1.readline()
        f1.close()
        length = len(line)
        assert False
        assert length > 3
