import unittest
import os

from decimal import Decimal
from sqlite3 import dbapi2 as sqlite
from events import SNmanager as snmanager
from events import globals

TESTFILE = "testdb2"

class TestSNmanager(unittest.TestCase):

    def setUp(self):
        if os.path.exists(TESTFILE): 
            os.remove(TESTFILE)

    def test_detele_irrelevant_users(self):
        
        # Function to test
        snmanager.delete_irrelevant_users()
        
        # Check the results
        con=sqlite.connect(TESTFILE)
        u = con.execute("SELECT fromid, toid FROM mentions WHERE fromid=? AND toid=?" 
                        , (1, 2)).fetchone()
        assert  u == (1,2)

    def test_addretweet(self):

        # Function to test
        self.dbmanager.addretweet(1, 2)

        # Check the results
        con=sqlite.connect(TESTFILE)
        u = con.execute("SELECT fromid, toid FROM retweets WHERE fromid=? AND toid=?" 
                        , (1, 2)).fetchone()
        print u
        assert u == (1,2)

    def test_addfavorite(self):
        self.dbmanager.addfavorite(1, 2)

        con=sqlite.connect(TESTFILE)
        u = con.execute("SELECT fromid, toid FROM favorites WHERE fromid=? AND toid=?" 
                        , (1, 2)).fetchone()        
        assert u == (1,2)


    def test_decay(self):
        self.dbmanager.addmention(1,2)
        self.dbmanager.addmention(1,2)
        self.dbmanager.addmention(1,2)
        
        self.dbmanager.decay(1,"mentions", 0.9)

        con=sqlite.connect(TESTFILE)
        u = con.execute("SELECT toid, count FROM mentions WHERE fromid=? AND toid=?"
                        , (1, 2)).fetchone()

        print u
        assert round(u[1],2)==0.3

    def test_gettrust(self):

        # Create graph
        self.dbmanager.addmention(1, 2)    
        self.dbmanager.addretweet(2, 1)
        
        # Function to test
        assert self.dbmanager.gettrust(1,2) == 0.5
        assert self.dbmanager.gettrust(2,1) == 0.5
        assert self.dbmanager.gettrust(1,1) == 0.5
        assert self.dbmanager.gettrust(2,2) == 0.5

    def test_init_trust_matrix(self):

        # Create graph
        self.dbmanager.addmention(1, 2)
        self.dbmanager.addmention(10,11)

        # Function to test
        self.dbmanager.init_trust_matrix()

        # Check the results
        con = sqlite.connect(TESTFILE)
        (trust,) = con.execute("SELECT count FROM transitions WHERE fromid=? AND toid=?"
                                 , (1, 2)).fetchone()
        assert trust == 0.5

    def test_trust_matrix_pow(self):

        # Create graph
        self.dbmanager.addmention(1,2)
        self.dbmanager.addmention(2,3)
        self.dbmanager.addmention(3,4)
        self.dbmanager.addmention(4,8)
        self.dbmanager.addmention(3,2)
        self.dbmanager.addmention(3,4)
        self.dbmanager.addmention(4,3)
        self.dbmanager.addmention(3,7)
        self.dbmanager.addmention(7,3) 
        self.dbmanager.addmention(15,16)

        # Function to test
        self.dbmanager.trust_matrix_pow(3)
    
        # Check the results
        con = sqlite.connect(TESTFILE)

        outputs = con.execute("SELECT count FROM temp WHERE fromid=?", (1,)).fetchall()
        totaloutput = 0
        for output in outputs:
            totaloutput = totaloutput + output[0]        
        assert round(totaloutput,2) == 1.0

        outputs = con.execute("SELECT count FROM temp WHERE fromid=?", (2,)).fetchall()
        totaloutput = 0
        for output in outputs:
            totaloutput = totaloutput + output[0]
        assert round(totaloutput,2) == 1.0

        outputs = con.execute("SELECT count FROM temp WHERE fromid=?", (3,)).fetchall()
        totaloutput = 0
        for output in outputs:
            totaloutput = totaloutput + output[0]
        assert round(totaloutput,2) == 1.0


    def test_calculate_trust_matrix(self):

        # Create graph
        self.dbmanager.addmention(1,2)
        self.dbmanager.addmention(2,3)
        self.dbmanager.addmention(3,1)

        # Function to test
        self.dbmanager.calculate_trust_matrix(3)

        # Check the results
        con = sqlite.connect(TESTFILE)
        (trusta,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (1,1)).fetchone()
        (trustb,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (1,2)).fetchone()
        (trustc,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (1,3)).fetchone()
        (trustd,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (2,1)).fetchone()
        (truste,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (2,2)).fetchone()
        (trustf,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (2,3)).fetchone()
        (trustg,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (3,1)).fetchone()
        (trusth,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (3,2)).fetchone()
        (trusti,) = con.execute("SELECT count FROM trust WHERE fromid=? AND toid=?"
                                , (3,3)).fetchone()

        print "Trust a", trusta
        print "Trust b", trustb
        print "Trust c", trustc
        print "Trust d", trustd
        print "Trust e", truste
        print "Trust f", trustf
        print "Trust g", trustg
        print "Trust h", trusth
        print "Trust i", trusti


        assert round(trusta,2) == 0.39
        assert round(trustb,2) == 0.48
        assert round(trustc,2) == 0.13
        assert round(trustd,2) == 0.13
        assert round(truste,2) == 0.39
        assert round(trustf,2) == 0.48
        assert round(trustg,2) == 0.48
        assert round(trusth,2) == 0.13
        assert round(trusti,2) == 0.39


        #   a1*A + a2*A*A + a3*A*A*A

        #   0.39286   0.48214   0.12500
        #   0.12500   0.39286   0.48214
        #   0.48214   0.12500   0.39286
