import unittest
import os

import events.usermodel

from events import globals
from events import utils

class TestUserModel(unittest.TestCase):
    
    def setUp(self):
        self.user = 'alberto_lm'
        
    def learn_model(self):
        
        # UserModel will have inheritance: BayesianModel, NeuralModel and so forth
        examples = utils.get_examples(self.user)
        model = UserModel(self.user, examples)
        y = model.predict(user, tweet)
        assert y>0
        assert y<1
