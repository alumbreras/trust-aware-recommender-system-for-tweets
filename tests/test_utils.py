# -*- coding: utf-8 -*-

import unittest
from os.path import *
from events import utils

class TestUtils(unittest.TestCase):

    def test_get_users_from_file(self):

        users_a = ['alice', 'bob', 'charles', 'david', 'eric' ]
        user = 'fred'


        filename = join(utils.basepath(), user + ".inftest")
        utils.set_users_to_file(filename, users_a)
        users_b = utils.get_users_from_file(filename)
        assert users_a == users_b
        
    def get_tweet_pos(self):

        user = 'alberto_lm'
        status = "Teddy Bautista cobrara una pension mensual de 24.000 euros (60% de su sueldo)"

        pos = utils.get_tweet_pos(user, status)
        
        print "Position:", pos
        assert pos > 1

    def get_tweet(self):
        
        user = 'alberto_lm'
        pos = 62
        status = utils.get_tweet(user, pos)
        print "Status retrieved:\n", status
        assert "Teddy Bautista" in status

    def get_examples(self):
        
        user = 'alberto_lm'
        examples = utils.get_examples(user)
        print "POS. examples", len(examples[0])
        print "NEG. examples", len(examples[1])
        assert len(examples[0])>0
        assert len(examples[1])>0

    def test_urldelete(self):
        tweet = "RT @vicentejuan: La GeneralitaValenciana http://borrar.com excluye a los hospitales públicos, de gestión privada, del cierre de camas http://t.co/Nms4DfCZ "
        r = utils.urldelete(tweet)
        print "Original:\n", tweet
        print "Limpio:\n", r
        assert False

    def test_usernamedelete(self):
        tweet = "RT @vicentejuan: La GeneralitaValenciana http://borrar.com excluye a los hospitales públicos, de gestión privada, @borrar del cierre de camas http://t.co/Nms4DfCZ "
        r = utils.usernamedelete(tweet)
        print "Original:\n", tweet
        print "Limpio:\n", r
        assert False

    def test_RTdelete(self):
        tweet = "RT @vicentejuan: La GeneralitaValenciana http://borrar.com excluye a los hospitales públicos, de gestión privada, @borrar del cierre de camas http://t.co/Nms4DfCZ "
        r = utils.RTdelete(tweet)
        print "Original:\n", tweet
        print "Limpio:\n", r
        assert False

    def test_hashtagdelete(self):
        tweet = "RT @vicentejuan: La #GeneralitaValenciana http://borrar.com excluye a los hospitales públicos, de gestión privada, @borrar del cierre de camas http://t.co/Nms4DfCZ #hashtag "
        r = utils.hashtagdelete(tweet)
        print "Original:\n", tweet
        print "Limpio:\n", r
        assert False
