import unittest
import os

from decimal import Decimal
from sqlite3 import dbapi2 as sqlite
from events import dbmanager
from events import usersmanager
from events import globals
from events import utils

TESTFILE = "testdb2"

class TestUsersManager(unittest.TestCase):

    def setUp(self):
        #if os.path.exists(TESTFILE): 
        #    os.remove(TESTFILE)
        #self.dbmanager = dbmanager.DBmanager(TESTFILE)
        globals.USERS_FILE = 'users_file.test'
        globals.USERS_SEED_FILE = 'users_seed_file.test'
        globals.USERS_DISCOVERED_FILE1 = 'users_discovered_file1.test'
        globals.USERS_DISCOVERED_FILE2 = 'users_discovered_file2.test'

    def tearDown(self):
        #self.dbmanager.dbcommit()
        #if os.path.exists(TESTFILE):
        #    os.remove(TESTFILE)
        pass
                
    def test_is_user(self):
        
        users = ['alice', 'bob']

        file = open(globals.USERS_SEED_FILE, 'w')
        for candidate in users:
            file.write(str(candidate) + "\n")
        file.close()

        # Function to test
        assert usersmanager.is_user('alice') == True
        assert usersmanager.is_user('fake_alice') == False

    def test_get_users(self):
        
        users = ['anthony', 'brian']
        users_seed = ['alice', 'bob']
        users_disc1 = ['charles','dave']
        users_disc2 = ['eve','fred']

        utils.set_users_to_file(globals.USERS_FILE, users)
        utils.set_users_to_file(globals.USERS_SEED_FILE, users_seed)
        utils.set_users_to_file(globals.USERS_DISCOVERED_FILE1, users_disc1)
        utils.set_users_to_file(globals.USERS_DISCOVERED_FILE2, users_disc2)

        users = usersmanager.get_users()
        assert len(users) == 8
        assert 'bob' in users
        assert 'boob' not in users
        
    def test_delete_user(self):
            
        users = ['anthony', 'brian']
        users_seed = ['alice', 'bob']
        users_disc1 = ['charles','dave']
        users_disc2 = ['eve','fred']
        
        utils.set_users_to_file(globals.USERS_FILE, users)
        utils.set_users_to_file(globals.USERS_SEED_FILE, users_seed)
        utils.set_users_to_file(globals.USERS_DISCOVERED_FILE1, users_disc1)
        utils.set_users_to_file(globals.USERS_DISCOVERED_FILE2, users_disc2)

        users = usersmanager.get_users()
        assert 'bob' in users
        usersmanager.delete_user('bob')
        users = usersmanager.get_users()
        assert 'bob' not in users
        assert 'alice' in users
        assert 'eve' in users
